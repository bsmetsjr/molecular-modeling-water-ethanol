# -*- coding: utf-8 -*-
"""
Created on Sat Feb  2 11:07:00 2019

@author: s149259

Testing the water-ethanol mixture.
Run and then load the resulting file into VMD and set the boundary box as:
    pbc set {30.0 30.0 30.0} -all
    pbc box -on
    pbc join res -all
"""

import waterethanolmix as mix
import integrators
import utils

from tqdm import tqdm # cool progress bar

boxsize=3
nWaterMolecules =4**3
nEtanolMolecules=2
qs, vs = mix.createInitialQV(boxsize,nWaterMolecules,nEtanolMolecules,300)

dt = 1.0e-3
t_end = 0.5
qst = [qs]
vst = [vs]

for i in tqdm(range(int(t_end/dt)), desc='Simulating ', ncols=80):
    qs, vs = integrators.velocityVerlet(qs, vs, mix.accelerationFunction, dt, boxsize)
    qst.append(qs)
    vst.append(vs)

utils.Output_xyz2(qst, 'test_mix.xyz',[["O","H","H"],['C','H','H','H','C','H','H','O','H']])




import matplotlib.pyplot as plt
import water
import numpy as np

trackV=np.array([water.avgK(vst[t][0]) for t in range(190,200,1) ])
plt.plot(trackV)
trackMaxV=np.array([np.max(vst[t][0]) for t in range(180,200,1) ])
plt.plot(trackMaxV) # explodes at t=197

t=196
qmix, vmix = integrators.velocityVerlet(qst[t],vst[t],mix.accelerationFunction,dt,boxsize)
water.avgK(vmix[0])
a=mix.accelerationFunction(qmix,vmix,boxsize)
np.max(a[0])
aWater=water.accelerationFunction([qmix[0]],[vmix[0]],2*boxsize/3)
np.max(aWater[0])

#specific run of water, starting from same config, but using water.accelFunct
Qs, Vs= [qst[0][0]], [vst[0][0]]
Qst, Vst = [Qs], [Vs]
for i in tqdm(range(int(t_end/dt)), desc='Simulating ', ncols=80):
    Qs, Vs = integrators.velocityVerlet(Qs, Vs, 
        water.accelerationFunction, dt, 2*boxsize/3)
    Qst.append(Qs)
    Vst.append(Vs)

trackMaxV=np.array([np.max(Vst[t][0]) for t in range(180,200,1) ])
plt.plot(trackMaxV) # explodes at t=197



# different implementation of computing accelFunct. for water
a1=mix.accelerationFunction(qst[0],vst[0],boxsize)
a2=water.accelerationFunction([qst[0][0]],[vst[0][0]],2*boxsize/3)

trackA=np.array([np.max(mix.accelerationFunction(qst[t],vst[t],boxsize)[0] - 
                        water.accelerationFunction([qst[t][0]],[vst[t][0]],2*boxsize/3)[0]) for t in range(180,200,1)])
plt.plot(trackA) # differen implementations of accel for water don't seem to differ.

trackA2=np.array([np.max(mix.accelerationFunction(qst[t],vst[t],boxsize)[0]) for t in range(180,200,1)])
plt.plot(trackA2) # both still tend to explode.

trackA3=np.array([np.max(water.accelerationFunction([qst[t][0]],[vst[t][0]],2*boxsize/3)[0]) for t in range(180,200,1)])
plt.plot(trackA3)


# considering the integrator w.r.t. their velocity output
# at each time, running integrato with input qst and vst, then selecting velocity of water and taking max of difference.
trackInt=np.array([
        np.max(integrators.velocityVerlet(qst[t],vst[t],mix.accelerationFunction,dt,boxsize)[1][0]-
               integrators.velocityVerlet([qst[t][0]],[vst[t][0]],water.accelerationFunction,dt,boxsize*2/3)[1][0]) 
        for t in range(180,200,1)])
plt.plot(trackInt)


trackInt2=np.array([
        np.max(integrators.velocityVerlet(qst[t],vst[t],mix.accelerationFunction,dt,boxsize)[1][0]) 
        for t in range(180,200,1)])
plt.plot(trackInt2)

trackInt3=np.array([
        np.max(integrators.velocityVerlet([qst[t][0]],[vst[t][0]],water.accelerationFunction,dt,boxsize*2/3)[1][0]) 
        for t in range(180,300,1)])
plt.plot(trackInt3)


# checking the createQV
np.max(water.createInitialQV(2,12,300)[1][0]-mix.createInitialQV(3,12,2,300)[1][0])