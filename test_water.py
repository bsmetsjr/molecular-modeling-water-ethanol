# -*- coding: utf-8 -*-
"""
Created on Thu Jan 31 19:17:30 2019

@author: bsmetsjr

Run and then load the resulting file into VMD and set the boundary box as:
    pbc set {10.0 10.0 10.0} -all
    pbc box -on
    pbc join res -all
"""
import numpy as np

import integrators
import water
import utils

from tqdm import tqdm # cool progress bar

boxsize = 2.0 #nm
nWater = int(33*boxsize**3)
T = 250 #K
q,v = water.createInitialQV(boxsize, nWater, T)
qs = [q]
vs = [v]

dt = 1.0
t_end = 1.0e4
qst = [qs]
vst = [vs]

for i in tqdm(range(int(t_end/dt)), desc='Simulating ', ncols=80):
    qs, vs = integrators.velocityVerlet(qs, vs, 
        water.accelerationFunction, dt, boxsize)
    qs, vs = water.isokineticThermostat(qs[0], vs[0], T)
    qs, vs = [qs], [vs]
    qst.append(qs)
    vst.append(vs)


utils.Output_xyz(qst, 'test_water.xyz', ['O','H','H'])
print('Done, results in test_water.xyz')















