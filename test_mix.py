# -*- coding: utf-8 -*-
"""
Created on Sat Feb  2 19:32:36 2019

@author: bsmetsjr
"""

import waterethanolmix as mix
import integrators
import utils
import numpy as np

from tqdm import tqdm # cool progress bar

boxsize=3.0 #nm
T = 300 #K

vvPercentageEthanol = 0.135
nWaterMolecules = int(((1-vvPercentageEthanol)*33*boxsize**3))
nEthanolMolecules = int((vvPercentageEthanol*10*boxsize**3))

qs, vs = mix.createInitialQV2(boxsize, nWaterMolecules,nEthanolMolecules, T)

dt = 1.0 #fs
t_end = 1.0e4 #fs
qst = [qs]
vst = [vs]

for i in tqdm(range(int(t_end/dt)), desc='Simulating ', ncols=80):
    qs, vs = integrators.velocityVerlet(qs, vs, mix.accelerationFunction, dt, boxsize)
    if np.mod(i,5) == 0:
        qs, vs = mix.isokineticThermostat(qs, vs, T)
    qst.append(qs)
    vst.append(vs)

utils.Output_xyz2(qst, 'test_mix.xyz',[["O","H","H"],['C','H','H','H','C','H','H','O','H']])


print('Done, results in test_mix.xyz')
