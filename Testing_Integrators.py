# -*- coding: utf-8 -*-
"""
Created on Wed Jan 16 10:30:03 2019

This script is designed for testing the following integrators on a single Hydrogen molecule:
- Euler,
- Leapfrog, and
- Velocity Verlet.
It outputs the figures used in chapter 2 of the report.

Make sure the file "integrators.py" is in the same directorty as this one.
"""

import numpy as np
import integrators
import matplotlib.pyplot as plt
##########################################################
# First, some Hydrogen specific functions are defined
##########################################################
r_b, K_b=0.1, 10 # in nm, and kJ/mol resp.
m=1.008 # mass of H atom in amu

def Force_Bonded(q1, q2):# r is norm of distance vector between two atoms
    """
Computing the bonded-force on atom q1, along the bond from q1 to q2.
INPUT:
    3-array of xyz coordinates of position of first atom,
    3-array of xyz coordinates of position of second atom,
    scalar representing equilibrium bond length,
    scalar representing spring constant
OUTPUT:
    3-array of xyz coordinates of bonded-force on first atom.
    """
    r=np.linalg.norm(q1-q2)
    return K_b*(r-r_b)/r*(q2-q1)


def accelerationFunction_H(Q,V,*args):
    """
accelerationFunction specific for Hydrogen molecule.
INPUT: 
    list of a single n,k,3-array of the positions of the atoms, where 
            n is the number of molecules,
            k is number of atoms per molecules,
            3 stands for the xyz-coordinates of the position.
OUTPUT:
    list of a single n,k,3-array of the accelerations of the atoms, where 
            n is the number of molecules,
            k is number of atoms per molecules,
            3 stands for the xyz-coordinates of the acceleration.
    """
    # only considering bond forces, with r_b=1 and K_b=100
    molecule=Q[0][0]# in nm, and kJ/mol resp.
    M=np.array([1.008,1.008]) # masses of H-atom in amu.
    F=np.array([[Force_Bonded(molecule[0],molecule[1]),
                Force_Bonded(molecule[1],molecule[0])]])
    return [F/M[:,np.newaxis]]#(1e-6)*
  


def Hamiltonian_H(qs,vs):
    """
Computing the Hamiltonian over a trajectory
INPUT:
    qs= T,1-list of the trajectory with elements n,k,3-array of the position of the atoms, where
            T is the number of evolution steps,
            n is the number of molecules,
            k is number of atoms per molecules,
            3 stands for the xyz-coordinates of the position.
    vs= T,1-list of the trajectory with elements n,k,3-array of the velocities of the atoms, where
            T is the number of evolution steps,
            n is the number of molecules,
            k is number of atoms per molecules,
            3 stands for the xyz-coordinates of the position.
OUTPUT:
    a T-array of the time evolution of the Hamiltonian, where
        T is the number of evolution steps.
    """
    H=np.array([])
    for t in range(len(qs)):
        q=qs[t][0]
        E=np.sum(m*(vs[t][0])**2)/2 + K_b*(np.linalg.norm(q[0,0]-q[0,1])-r_b)**2/2 # kinetic plus potential energy, both in kJ/mol
        H=np.append(H,E)   
    return H

def Kinetic_H(vs):
    """
    Computing the kinetic energy
    input:
        a T,1-list of the trajectory with elements n,k,3-array of the velocities of the atoms
    output:
        a T-array of the time evolution of the kinetic energy.
    """
    Ekin=np.array([])
    for t in range(len(vs)):
        E=np.sum(m*(vs[t][0])**2)/2
        Ekin=np.append(Ekin,E)
    return Ekin

def Potential_H(qs):
    Epot=np.array([])
    for t in range(len(qs)):
        q=qs[t][0]
        E=K_b*(np.linalg.norm(q[0,0]-q[0,1])-r_b)**2/2
        Epot=np.append(Epot,E)
    return Epot

def velocityVerlet2(qs, vs, accelerationFunction, dt):
    """
    Velocity Verlet, without boxsize bounds (in contrast with integrators.velocityVerlet)
    
    inputs:
        qs: python list per species of np.array[..., #atom, (x,y,z)]
        vs: python list per species of np.array[..., #atom, (vx,vy,vz)]
        
        accelerationFunction: function that takes (q,v) as argument and returns:
            python list per species np.array[..., #atom, (ax,ay,az)]
            
        dt: float (timestep)
    return:
        (qs_next, vs_next)
    """
    assert len(qs)==len(vs)
    a = accelerationFunction(qs, vs)
    qsnew = [qs[i] + dt*vs[i] + (dt**2)/2*a[i] for i in range(len(qs))]
    
    anew = accelerationFunction(qsnew, vs)
    vsnew = [vs[i] + dt/2*(a[i]+anew[i]) for i in range(len(qs))]
    
    return qsnew, vsnew
##########################################################
# From here on, different Integrators scheme are tested on their Hamiltonian conservation.
# Initially, running the Integrator schemes.
##########################################################

dt=0.001 # time in ps
t_end=50
steps=int(t_end/dt) # running until 0.2 ns, with steps of 1 fs

q_0=[np.array([[[-0.2,0,0],[0.2,0,0]]])] # initial positions of H-atoms in nm
v_0=[np.array([[[0,0,0],[0,0,0]]])] # initial velocity in nm/ps

q_Euler=[q_0]
v_Euler=[v_0]
for i in range(steps):
    Q,V=integrators.euler(q_Euler[i],v_Euler[i],accelerationFunction_H,dt)
    q_Euler.append(Q)
    v_Euler.append(V)


q_Leapfrog=[q_0]
v_Leapfrog=[v_0]
for i in range(steps):
    Q,V=integrators.leapfrog(q_Leapfrog[i],v_Leapfrog[i],accelerationFunction_H,dt) # 
    q_Leapfrog.append(Q)
    v_Leapfrog.append(V)

q_VelocityVerlet=[q_0]
v_VelocityVerlet=[v_0]
for i in range(steps):
    Q,V=velocityVerlet2(q_VelocityVerlet[i],v_VelocityVerlet[i],accelerationFunction_H,dt)
    q_VelocityVerlet.append(Q)
    v_VelocityVerlet.append(V)
##############################################################
# Computing the Hamiltonian or Kinetic-Potential energy over time, and visualizing it in a figure
##############################################################
H_Euler=Hamiltonian_H(q_Euler,v_Euler)
plt.plot(H_Euler,label="Euler") # Hamitonian over time with Euler integrator
plt.xlabel("time evolution with stepsize {}".format(dt))
plt.ylabel("energy in kJ/mol")
plt.legend(loc=2)
axes=plt.gca()
axes.set_ylim([0.3,1])

H_VelocityVerlet=Hamiltonian_H(q_VelocityVerlet,v_VelocityVerlet)
plt.plot(H_VelocityVerlet,label="Velocity Verlet") # Hamitonian over time with  VVerlet integrator
plt.legend(loc=1)
plt.xlabel("time evolution with stepsize {}".format(dt))
plt.ylabel("energy in kJ/mol")
axes=plt.gca()
axes.set_ylim([0,1])

Ekin_Leapfrog=Kinetic_H(v_Leapfrog)
plt.plot(Ekin_Leapfrog,label="Leapfrog, Kinetic energy",linestyle="--")
plt.plot(np.ones(len(Ekin_Leapfrog))*Epot_Leapfrog[0],label="initial potential energy")
plt.legend(loc=2)
plt.xlabel("time evolution with stepsize {}".format(dt))
plt.ylabel("energy in kJ/mol")
axes=plt.gca()
axes.set_ylim([0,1])

Epot_Leapfrog=Potential_H(q_Leapfrog)
plt.plot(Epot_Leapfrog,label="Leapfrog, Potential energy",linestyle="--")
plt.plot(np.ones(len(Epot_Leapfrog))*Epot_Leapfrog[0],label="initial potential energy")
plt.legend(loc=2)
plt.xlabel("time evolution with stepsize {}".format(dt))
plt.ylabel("energy in kJ/mol")
axes=plt.gca()
axes.set_ylim([0,1])

H_Leapfrog=Hamiltonian_H(q_Leapfrog,v_Leapfrog)
plt.plot(H_Leapfrog,label="Leapfrog Verlet") # approximation of Hamitonian over time with  Leapfrog integrator
plt.legend(loc=1)
plt.xlabel("time evolution with stepsize {}".format(dt))
plt.ylabel("energy in kJ/mol")
axes=plt.gca()
axes.set_ylim([0,1])

# difference between max(H) and min(H)
