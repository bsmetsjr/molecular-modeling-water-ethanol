# -*- coding: utf-8 -*-
"""
Created on Wed Feb  6 10:15:33 2019

This file conducts an analysis on the computed trajectories in main.py
Specifically, the O-O radial distances are computed for the last 2000 time steps.

Place the map "XYZ" (containing .xyz files produced by main.py) in the working directory.

Water molecules 33/nm^3, ethanol molecules 10/m^3
36 ethanol, 770 water
"""

import numpy as np
import utils
import geometry as g
import matplotlib.pyplot as plt
import water
from tqdm import tqdm


dt = 1.0 # in fs
boxsize = 3.0 # in nm

nWaterMolecules = int(33*boxsize**3)

frames_to_skip=8001

"""
Constructing O-O radial distances for water
"""
q, N = utils.readTrajectorySkip("XYZ\main_water_q.xyz", frames_to_skip*(nWaterMolecules*3+2),"O") # the last part of the O-atoms are loaded
q=np.reshape(q,[len(q),nWaterMolecules,3]) # reshape to [#frames, #O_atoms, (x,y,z)]

# per time frame, gathering all the distances between any two O-elements by considering only the upperpart of distant-matrix.
nO_atoms=len(q[0])*(len(q[0])-1)/2 # number of non-trivial distances between all the O-particles
O_average=np.array([np.sum(g.allPeriodicDistances(q[t],boxsize))/nO_atoms for t in range(len(q))])

plt.plot(O_average,label="radial distance of O-O in Water")
plt.xlabel("time steps with dt= {}, starting from t={}".format(dt, frames_to_skip))
plt.ylabel("distance in nm")
plt.legend(loc=1)


"""
Constructing radial distances for ethanol.
"""
nEthanolMolecules = int(10*boxsize**3)

q, N = utils.readTrajectorySkip("XYZ\main_ethanol_q.xyz", frames_to_skip*(nEthanolMolecules*9+2),"O") # the last part of the O-atoms are loaded
q=np.reshape(q,[len(q),nEthanolMolecules,3]) # reshape to [#frames, #O_atoms, (x,y,z)]

nO_atoms=len(q[0])*(len(q[0])-1)/2 # number of non-trivial distances between all the O-particles
O_average=np.array([np.sum(g.allPeriodicDistances(q[t],boxsize))/nO_atoms for t in range(len(q))])

plt.plot(O_average,label="radial distance of O-O in ethanol")
plt.xlabel("time steps with dt= {}, starting from t={}".format(dt, frames_to_skip))
plt.ylabel("distance in nm")
plt.legend(loc=1)


"""
Constructing radial distances for 13.5 % ethanol-water mixture.
"""
vvPercentageEthanol = 0.135
nWaterMolecules = int((1-vvPercentageEthanol)*33*boxsize**3)
nEthanolMolecules = int(vvPercentageEthanol*10*boxsize**3)

q, N = utils.readTrajectorySkip("XYZ\main_mix_q.xyz", frames_to_skip*(nWaterMolecules*3+nEthanolMolecules*9+2),"O") # the last part of the O-atoms are loaded
q=np.reshape(q,[len(q),nWaterMolecules+nEthanolMolecules,3]) # reshape to [#frames, #O_atoms, (x,y,z)]


nO_atoms=len(q[0])*(len(q[0])-1)/2 # number of non-trivial distances between all the O-particles
O_average=np.array([np.sum(g.allPeriodicDistances(q[t],boxsize))/nO_atoms for t in range(len(q))])

plt.plot(O_average,label="radial distance of O-O in ethanol-water mix")
plt.xlabel("time steps with dt= {}, starting from t={}".format(dt, frames_to_skip))
plt.ylabel("distance in nm")
plt.legend(loc=1)



"""
Radial distribution functions for ethanol O-O
"""
vvPercentageEthanol = 0.135
nWaterMolecules = int((1-vvPercentageEthanol)*33*boxsize**3)
nEthanolMolecules = int(vvPercentageEthanol*10*boxsize**3)

q_mix, N = utils.readTrajectorySkip("XYZ\main_mix_q.xyz", frames_to_skip*(nWaterMolecules*3+nEthanolMolecules*9+2),"O") # the last part of the O-atoms are loaded
q_mix=np.reshape(q_mix,[len(q_mix),nWaterMolecules+nEthanolMolecules,3]) # reshape to [#frames, #O_atoms, (x,y,z)]

qOmix = q_mix[:,-nEthanolMolecules:]
dists_mix = [g.distanceTensor(g.periodicRelativePositionTensor(qOmix[i], boxsize)) for i in range(qOmix.shape[0])]
dists_mix=np.array(dists_mix)
rdOmix = dists_mix.flatten()
rdOmix = rdOmix[rdOmix.nonzero()]

nEthanolMolecules = int(10*boxsize**3)
q_pure, N = utils.readTrajectorySkip("XYZ\main_ethanol_q.xyz", frames_to_skip*(nEthanolMolecules*9+2),"O") # the last part of the O-atoms are loaded
q_pure=np.reshape(q_pure,[len(q_pure),nEthanolMolecules,3]) # reshape to [#frames, #O_atoms, (x,y,z)]

qOpure = q_pure
dists_pure = [g.distanceTensor(g.periodicRelativePositionTensor(qOpure[i], boxsize)) for i in range(qOpure.shape[0])]
dists_pure=np.array(dists_pure)
rdOpure = dists_pure.flatten()
rdOpure = rdOpure[rdOpure.nonzero()]


plt.hist((rdOpure,rdOmix), bins=100, density=True, histtype='step', label=['Pure', 'Solution'])
plt.legend(loc='upper right')
plt.show()



"""
Radial distribution functions for ethanol C(1)-C(1)
"""
vvPercentageEthanol = 0.135
nWaterMolecules = int((1-vvPercentageEthanol)*33*boxsize**3)
nEthanolMolecules = int(vvPercentageEthanol*10*boxsize**3)

q_mix, N = utils.readTrajectorySkip("XYZ\main_mix_q.xyz", frames_to_skip*(nWaterMolecules*3+nEthanolMolecules*9+2),"C") # the last part of the O-atoms are loaded
q_mix=np.reshape(q_mix,[len(q_mix),nEthanolMolecules,2,3]) # reshape to [#frames, #O_atoms, (x,y,z)]

qC1mix = q_mix[:,:,0,:]
dists_mix = [g.distanceTensor(g.periodicRelativePositionTensor(qC1mix[i], boxsize)) for i in range(qC1mix.shape[0])]
dists_mix=np.array(dists_mix)
rdC1mix = dists_mix.flatten()
rdC1mix = rdC1mix[rdC1mix.nonzero()]

nEthanolMolecules = int(10*boxsize**3)
q_pure, N = utils.readTrajectorySkip("XYZ\main_ethanol_q.xyz", frames_to_skip*(nEthanolMolecules*9+2),"C") # the last part of the O-atoms are loaded
q_pure=np.reshape(q_pure,[len(q_pure),nEthanolMolecules,2,3]) # reshape to [#frames, #O_atoms, (x,y,z)]

qC1pure = q_pure[:,:,0,:]
dists_pure = [g.distanceTensor(g.periodicRelativePositionTensor(qC1pure[i], boxsize)) for i in range(qC1pure.shape[0])]
dists_pure=np.array(dists_pure)
rdC1pure = dists_pure.flatten()
rdC1pure = rdC1pure[rdC1pure.nonzero()]


plt.hist((rdC1pure,rdC1mix), bins=100, density=True, histtype='step', label=['Pure', 'Solution'])
plt.legend(loc='upper right')
plt.show()



"""
Water energy plot
"""
nWater = int(33*boxsize**3)
q, N = utils.readTrajectory('XYZ\main_water_q.xyz')
v, N = utils.readTrajectory('XYZ\main_water_v.xyz')
q = np.array(q)
v = np.array(v)

q = q.reshape(len(q), nWater, 3, 3)/10.0
v = v.reshape(len(v), nWater, 3, 3)/10.0


T = np.array([water.temperature(v[i]) for i in range(len(v))])
avgK = np.array([water.avgK(v[i]) for i in range(len(v))])
K = avgK*3*nWater
V = np.array([water.potentialEnergy([q[i]], [v[i]], boxsize) for i in tqdm(range(0,len(q),50))])

V2 = np.sum(V, axis=-1)
V2 = V2 - V2[0] + K2[0]
T2 = T[range(0,len(q),50)]
K2 = K[range(0,len(q),50)]


plt.plot(np.linspace(0,10,len(V2)),V2, label='Potential')
plt.plot(np.linspace(0,10,len(V2)), K2, label='Kinetic')
plt.legend()
plt.show()










