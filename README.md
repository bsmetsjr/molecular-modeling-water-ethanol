# Molecular Modeling

This repository contains a molecular dynamics simulator made in the context of the **Introduction to Molecular Modeling and Simulation** (2MMN40) course at the Eindhoven University of Technology.

The code was tested with the following dependencies:

+ Python 3.7.2,
+ Numpy 1.15.4,
+ Matplotlib 3.0.2.
+ tqdm 4.29 (for progress bars)


## Running the Simulation

The following files contains scripts that setup a particular simulation and output the results as an XYZ file.

+ `main.py`: primary results used in the paper.
+ `test_water.py`, `test_ethanol.py`, `test_mix.py`: test particular setups.

## Results

The produced XYZ files can be visualized with VMD, see for example:

+ <https://youtu.be/d0JdvQecpXQ>,
+ <https://youtu.be/tixaxatnGS0>.

The code in `Analysis.py` allows more quantitive analysis, such as the following radial distribution plots:

![OOradial](OOradial.png)
![C1C1radial](C1C1radial.png)


## File Structure

Most parts of the simulation are built agnostic towards the type of molecule(s) that are being simulated. All molecule specific code is contained in `water.py` (for pure water), `ehtanol.py` (for pure ethanol) and `waterethanolmix.py` (for water/ethanol mixtures). 

The following files contain all the code not tied to specific molecule types:

+ `geometry.py`: geometrical helper functions, mainly force vector calculation for different types of potentials.
+ `integrators.py`: different types of time integrators.
+ `utils.py`: various utility functions such as reading/writing XYZ files.

Files prefixed with `test_` contain scripts testing out various parts of the code.

## Data Structures

For a given type of molecule we store position and velocity vectors in a numpy array, usually denoted `q` and `v`, which have shape `(nMolecules, nAtomsPerMolecule, 3)`.

For a mixture of several types we keep a python list per type usually called `qs` and `vs`, for a water/ethanol mixture we would have `qs = [qWater, qEthanol]` and `vs = [vWater, vEthanol]`.

For each timestep we again store these mixture lists in a python list, usually called `qst` and `vst`.

Every iteration we calculate the relative position tensor and the relative distance tensor both for all atoms together and intra-molecular:

+ `dr`: full relative position tensor with shape `(nAtoms, nAtoms, 3)`.
+ `dist`: full relative distance tensor with shape `(nAtoms, nAtoms)`.
+ `dri`: intra-molecular relative position tensor with shape `(nMolecules, nAtomsPerMolecule, nAtomsPerMolecule, 3)`.
+ `disti`: intra-molecular relative distance tensor with shape `(nMolecules, nAtomsPerMolecule, nAtomsPerMolecule)`.

And similar data structures are built for the interaction between species, for the water-ethanol interaction we have:

+ `drwe`: full relative position tensor with shape `(3*nWaterMolecules, 9*nEthanolMolecules, 3)`.
+ `distwe`: full relative distance tensor with shape `(3*nWaterMolecules, 9*nEthanolMolecules)` .

## License

Copyright 2019, B.M.N. Smets, A.A. Cotino.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.