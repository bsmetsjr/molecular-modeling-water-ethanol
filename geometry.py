# -*- coding: utf-8 -*-
"""
Created on Tue Nov 27 11:12:59 2018

@author: bsmetsjr

Geometrical helper functions.

"""
import numpy as np
import math



def relativePositionTensor(q):
    """
    Relative position tensor.
    input:
        q = np.array[..., #particle, (x, y, z)]
    output:
        np.array[..., #particle1, #particle2, (dx, dy, dz)]
    """
    return q[..., np.newaxis, :, :]-q[..., np.newaxis, :]


def mixedRelativePositionTensor(q1, q2):
    """
    Relative position tensor.
    input:
        q1 = np.array[#particle1, (x, y, z)]
        q2 = np.array[#particle2, (x, y, z)]
    output:
        np.array[..., #particle1, #particle2, (dx, dy, dz)]
    """
    return q2[None,:,:]-q1[:,None,:]




def periodicMixedRelativePositionTensor(q1, q2, boxsize):
    """
    Relative position tensor corrected for periodic boundary conditions 
    in a cube with size boxsize.
    input:
        q1 = np.array[..., #particle1, (x,y,z)]
        q2 = np.array[..., #particle2, (x,y,z)]
        boxsize = float
    output:
        np.array[..., #particle1, #particle2, (dx, dy, dz)]
    """
    dr = mixedRelativePositionTensor(q1, q2)
    return np.mod(dr-boxsize/2, boxsize)-boxsize/2




def periodicRelativePositionTensor(q, boxsize):
    """
    Relative position tensor corrected for periodic boundary conditions 
    in a cube with size boxsize.
    input:
        q = np.array[..., #particle, (x,y,z)]
        boxsize = float
    output:
        np.array[..., #particle1, #particle2, (dx, dy, dz)]
    """
    dr = relativePositionTensor(q)
    return np.mod(dr-boxsize/2, boxsize)-boxsize/2
        


def distanceTensor(dr):
    """
    Inter-atomic distances computed from the relative position tensors
    input: 
        dr = np.array[#...#, (dx, dy, dz)]
    output: 
        np.array[#...#, distance]
    """
    return np.linalg.norm(dr, axis=-1)


def allPeriodicDistances(Q,boxsize):
    """
    input:
        Q= np.array[#atoms, (x,y,z)]
        boxsize
    output:
        np.array of all the periodic distances between all the atoms in Q
    """
    return distanceTensor(periodicRelativePositionTensor(Q,boxsize))[np.triu_indices(len(Q),1)]



def massCenters(qm, w):
    """
    Molecular centers of mass
    input: 
        qm = np.array[#molecule, #atom, (x, y, z)]
        w = np.array[#atom] atomic weights
    output: 
        np.array[#molecule, (x, y, z)]
    """
    return np.matmul(w, qm)/np.sum(w)


def bondForce(dri, disti, i, j ,c):
    """
    Bond force vectors. Takes the intra-molecular relative position and distance tensor and gives the bond force vector between atom i and atom j given the stiffness constant K and rest distance R.
    input:
        dri = np.array[#molecule, #atom1, #atom2, (dx,dy,dz)]
        disti = np.array[#molecule, #atom1, #atom2, dist]
        i = #atom1
        j = #atom2
        c = list of stiffness constant K and resting distance R

    return:
        np.array[#molecule, 2, (Fx, Fy, Fz)]
    """
    f = c[1]*(c[0]-disti[...,i,j,None])*dri[...,i ,j, :]/disti[...,i,j, None]
    return np.concatenate((-f[..., None, :], f[..., None, :]), axis=-2)


def bondPotential(dri, disti, i, j ,c):
    """
    Bond potential. Takes the intra-molecular relative position and distance tensor and gives the bond force vector between atom i and atom j given the stiffness constant K and rest distance R.
    input:
        dri = np.array[#molecule, #atom1, #atom2, (dx,dy,dz)]
        disti = np.array[#molecule, #atom1, #atom2, dist]
        i = #atom1
        j = #atom2
        c = list of stiffness constant K and resting distance R

    return:
        np.array[#molecule, 1]
    """
    return 1/2*c[1]*(c[0]-disti[...,i,j])**2


def angleForce(dri, disti, i, j, k, c):
    """
    Angle force vectors. Takes the intra-molecular relative position tensor dri and gives the unscaled (spreading) force vector on atoms i,j and k where i is the central atom and the angle considered is that from j to i to k.
    input:
        dri = np.array[#molecule, #atom1, #atom2, (dx, dy, dz)]
        disti = np.array[#molecule, #atom1, #atom2, (dist)]
        i = central atom
        j = first 'leg' atom
        k = second 'leg' atom
        c = list of stiffness constant K and resting angle (radians) A

    return:
        np.array[#molecule, 3, (Fx, Fy, Fz)]
    """
    dr1 = dri[...,i,j,:]
    dr2 = dri[...,i,k,:]
    r1 = disti[...,i,j]
    r2 = disti[...,i,k]
    inner = np.sum(dr1*dr2, axis=-1, keepdims=True)
    
    theta = np.arccos(inner[:,0]/(r1*r2))
    
    f1 = (c[1]*(theta-c[0])/(r1*r2*np.sin(theta))).reshape(-1,1)*(dr2-dr1*inner/(r1**2).reshape(-1,1))
    f2 = (c[1]*(theta-c[0])/(r1*r2*np.sin(theta))).reshape(-1,1)*(dr1-dr2*inner/(r2**2).reshape(-1,1))
    
    fj = f1[..., None, :]
    fk = f2[..., None, :]
    fi = -fj-fk
    
    return np.concatenate((fi, fj, fk), axis=-2)



def anglePotential(dri, disti, i, j, k, c):
    """
    Angle potential. Takes the intra-molecular relative position tensor dri and gives the unscaled (spreading) force vector on atoms i,j and k where i is the central atom and the angle considered is that from j to i to k.
    input:
        dri = np.array[#molecule, #atom1, #atom2, (dx, dy, dz)]
        disti = np.array[#molecule, #atom1, #atom2, (dist)]
        i = central atom
        j = first 'leg' atom
        k = second 'leg' atom
        c = list of stiffness constant K and resting angle (radians) A

    return:
        np.array[#molecule]
    """
    dr1 = dri[...,i,j,:]
    dr2 = dri[...,i,k,:]
    r1 = disti[...,i,j]
    r2 = disti[...,i,k]
    inner = np.sum(dr1*dr2, axis=-1, keepdims=True)
    
    theta = np.arccos(inner[:,0]/(r1*r2))
    
    return 1/2*c[1]*(theta-c[0])**2




def dihedralForceRb(dri, disti, i, j, k, l, C):
    """
    Dihedral force vectors based on the Ryckaert-Bellman potential.
    """
    C1, C2, C3, C4 = C
    
    drji = dri[...,j,i,:]
    drjk = dri[...,j,k,:]
    drkl = dri[...,k,l,:]
    
    dji = disti[...,j,i]
    djk = disti[...,j,k]
    dkl = disti[...,k,l]
    
    n1 = np.cross(drji, drjk)
    n2 = np.cross(drkl, drjk)
    n1norm = np.linalg.norm(n1, axis=-1)
    n2norm = np.linalg.norm(n2, axis=-1)
    n1n = n1/n1norm[:,None]
    n2n = n2/n2norm[:,None]
    
    inner = np.clip(np.sum(n1n*n2n, axis=-1), -1.0, 1.0)
    theta = np.arccos(inner)
    
    sin_ijk = np.linalg.norm(np.cross(drji, drjk)) / (dji*djk)
    sin_jkl = np.linalg.norm(np.cross(drkl, -drjk)) / (dkl*djk)
    
    strength = C1/2.0*np.sin(theta) + C2*np.sin(2*theta)
    strength += 3.0/2.0*C3*np.sin(3*theta) + 2*C4*np.sin(4*theta)
    
    strength_i = strength / (dji*sin_ijk)
    strength_l = strength / (dkl*sin_jkl)
    
    fi = strength_i[:, None] * n1n
    fl = strength_l[:, None] * n2n
    
    fk = np.cross(drjk, fl)+np.cross(drkl, fl)+np.cross(drji, fi)
    fk =  -np.cross(fk, drjk)/(djk[:,None]**2)
    fj = -fi-fl-fk
    
    fi = fi[...,None,:]
    fj = fj[...,None,:]
    fk = fk[...,None,:]
    fl = fl[...,None,:]
    
    return np.concatenate((fi, fj, fk, fl), axis=-2)
    

def dihedralPotentialRb(dri, disti, i, j, k, l, C):
    """
    Dihedral potential based on the Ryckaert-Bellman potential.
    
    input:
        dri = np.array[#molecule, #atom1, #atom2, (dx, dy, dz)]
        disti = np.array[#molecule, #atom1, #atom2, (dist)]
        i = central atom
        j = first 'leg' atom
        k = second 'leg' atom
        c = list of stiffness constant K and resting angle (radians) A

    return:
        np.array[#molecule]
    """
    C1, C2, C3, C4 = C
    
    drji = dri[...,j,i,:]
    drjk = dri[...,j,k,:]
    drkl = dri[...,k,l,:]
    
    n1 = np.cross(drji, drjk)
    n2 = np.cross(drkl, drjk)
    n1norm = np.linalg.norm(n1, axis=-1)
    n2norm = np.linalg.norm(n2, axis=-1)
    n1n = n1/n1norm[:,None]
    n2n = n2/n2norm[:,None]
    
    inner = np.clip(np.sum(n1n*n2n, axis=-1), -1.0, 1.0)
    theta = np.arccos(inner)
    phi = theta - np.pi
    
    V = C1/2*(1+np.cos(phi)) + C2/2*(1-np.cos(2*phi))
    V += C3/2*(1+np.cos(3*phi)) + C4/2*(1-np.cos(4*phi))
    
    return V



def ljForce(dr, dist, A, B, cutoff=0.8):
    """
    LJ force between species.
        input:
            dr = np.array[#atom1, #atom2, (x,y,z)]
            dist = np.array[#atom1, #atom2]
            A = np.array[#atom1, #atom2] A constant between #1 and #2
            B = np.array[#atom1, #atom2] B constant between #1 and #2
            cutoff = float, cutoff distance
    """
    mask = np.logical_and(dist!=0, dist<cutoff)
    rminus14 = np.power(dist, -14, out=np.zeros_like(dist), where=mask)
    rminus8 = np.power(dist, -8, out=np.zeros_like(dist), where=mask)
    strength = -12*A*rminus14 + 6*B*rminus8
    return strength[...,None]*dr


def ljPotential(dr, dist, A, B, cutoff=0.8):
    """
    LJ force between species.
        input:
            dr = np.array[#atom1, #atom2, (x,y,z)]
            dist = np.array[#atom1, #atom2]
            A = np.array[#atom1, #atom2] A constant between #1 and #2
            B = np.array[#atom1, #atom2] B constant between #1 and #2
            cutoff = float, cutoff distance
    """
    mask = np.logical_and(dist!=0, dist<cutoff)
    rminus12 = np.power(dist, -12, out=np.zeros_like(dist), where=mask)
    rminus6 = np.power(dist, -6, out=np.zeros_like(dist), where=mask)
    S = np.divide(B**2, 4*A, out=np.zeros_like(dist), where=A!=0)
    return (A*rminus12 - B*rminus6)/2



def clampToBox(q, boxsize):
    """
        Put atoms outside of the box back inside the box.
    """
    return np.mod(q, boxsize)


def dihedralForce(dri, i, j, k, l, c):
    """
    Dihedral force vectors. takes the intra-molecular relative position tensor dri and gives 
    the force vector on atoms i,j, k and l where i and l where i is connected to j, j to k, and k to l (well defined, since we only consider proper dihedrals) 
    input:
        dri = np.array[#molecule, #atom1, #atom2, (dx, dy, dz)]
        i = first atom in chain
        j = second atom in chain
        k = third atom in chain
        l = fourth atom in chain
        c = list of the four dihedral constants c_i
    return:
        np.array([#molecules,4,(Fx,Fy,Fz)])
    """
    dr1=dri[...,i,j,:] # q1-q0
    dr2=dri[...,i,k,:] # q2-q0
    dr3=dri[...,l,j,:] # q1-q3
    dr4=dri[...,l,k,:] # q2-q3
    dr5=dri[...,k,j,:] # q1-q2
    n1=np.cross(dr1,dr2)
    n2=np.cross(dr3,dr4)
    norm1, norm2 =np.linalg.norm(n1,axis=1), np.linalg.norm(n2,axis=1)
    inProduct = np.sum(n1*n2, axis=-1, keepdims=True)
    
    theta=np.arccos(inProduct[:,0]/(norm1*norm2))-math.pi
    # rounding errors can cause |cos(theta)|>1,producing a "nan" value.
    if np.any(np.isnan(theta)): # guard against numerical rounding errors causing cos(theta)>1 or cos(theta)<-1
        theta=np.arccos(np.round(inProduct[:,0]/(norm1*norm2)))-math.pi
    
    coefficient= sum(c[i-1]*(i)*np.sin((i)*theta)*(-1)**i for i in range(1,5,1))/(2*np.sin(theta)*norm1*norm2)
    coefficient[theta==0]=0 # correcting for a division by zero. Setting F_dihedral=0 for theta=0 by definition.
    
    F1=np.cross(n2,dr5)-np.cross(n1,dr5)
    F2=np.cross(n1,dr4)+np.cross(n2,dr2)+np.cross(np.ones(3),n1*dr4+n2*dr2)-np.cross(n1,dr2)-np.cross(np.ones(3),n1*dr2)-np.cross(n2,dr4)-np.cross(np.ones(3),n2*dr4)
    F3=-np.cross(n1,dr3)-np.cross(n2,dr1)-np.cross(np.ones(3),n1*dr3+n2*dr1)+np.cross(n1,dr1)+np.cross(np.ones(3),n1*dr1)+np.cross(n2,dr3)+np.cross(np.ones(3),n2*dr3)
    F4=np.cross(n1,dr5)-np.cross(n2,dr5)
    return coefficient[:,np.newaxis,np.newaxis]*np.concatenate((F1[...,None,:],F2[...,None,:],F3[...,None,:],F4[...,None,:]),axis=-2)






def indexMonoLj(epsilon,i,j,n_atoms):
    """
    a function used in monoLjForce2. 
    input:
        epsilon = list of minimum energies of each atom in molecule (same input is of monoLjForce2)
        i = index of atom i (w.r.t. to all atoms)
        j = index of atom j
        n_atoms = number of atoms in a molecule
    return:
        scaling of epsilon's (if atom i and j are from a different molecule), or
        0 (if atom i and j are from same molecule)
        
    """
    if np.abs(i-j)>=n_atoms:
        return np.sqrt(epsilon[np.mod(i,n_atoms)]*epsilon[np.mod(j,n_atoms)])
    else: 
        return 0
   

def monoLjForce2(q,sigma,epsilon):
    """
    LJ force
    input:
        q = np.array([#molecule, #atom, (x,y,z)])
        sigma = list of equilibrium distances of each atom in molecule, e.g. sigma=[LjsigmaO,LjsigmaH,LjsigmaH]
        epsilon= list of minimum energies of each atom in molecule, e.g. epsilon=[LjepsilonO,LjepsilonH,LjepsilonH]
    return:
        np.array([#molecule,#atom,(Fx,Fy,Fz)])
    """
    Q=np.reshape(q,[len(q)*len(q[0]),3])
    dr=relativePositionTensor(Q)
    R=distanceTensor(dr)+np.diag(np.ones(len(Q))) # this is done to prevent dividing by zero. No effect on end result.
    n_atoms=len(q[0])
    
    S=np.array([[(sigma[np.mod(i,n_atoms)]+sigma[np.mod(j,n_atoms)])/2 for j in range(len(Q))] for i in range(len(Q))])
    E=np.array([[indexMonoLj(epsilon,i,j,n_atoms) for j in range(len(Q))] for i in range(len(Q))])
    f= 24*E[...,np.newaxis]*(S[...,np.newaxis]**6/R[...,np.newaxis]**(8)-2*S[...,np.newaxis]**12/R[...,np.newaxis]**14)*dr
    return  np.reshape(np.sum(f,axis=1),[len(q),len(q[0]),3]) # returns LJ forces of each atom in each molecule. 




    
        
    
    
    
    
    
    
    
    
    
    
    

