# -*- coding: utf-8 -*-
"""
Created on Wed Nov 21 10:57:44 2018

@author: bsmetsjr

General utilities such as read/writing XYZ files etc.
"""
import numpy as np
from tqdm import tqdm

# masses in amu
AtomicWeights = {
        "H": 1.0080,
        "O": 15.9994,
        "C": 12.0110
        }



"""
    Infer atoms per molecule by a jump in distance.
"""
def atomsPerMolecule(atoms):
    for i in range(2,10):
        a = atoms[0:i]
        
        avg_dist = np.average(np.linalg.norm(np.diff(a, axis=0), axis=1))
        #print("avg=", avg_dist, a.shape)
        next_dist = np.linalg.norm(atoms[i-1]-atoms[i])
        #print("d=", next_dist)
        
        if next_dist > 3*avg_dist:
            return i
        
        
        
"""
    Default ReadTrajectory implementation.
"""       
def readTrajectory(trajFile):
    """This will read the WHOLE trajectory into memory. This will not be possible
later very large trajectores. I'll let you worry about how to deal with that yourself..."""
    trajectory=[]
    with open(trajFile, "r") as tF:
        line = tF.readline()
        while line is not "":         
            #first line is number of atoms
            N = int(line.strip())
            #print("N = ", N)
            
            tF.readline().strip() # second line is a comment that we throw away

            q = []
            for i in range(N):            
                line = tF.readline().strip().split("\t")
                for c in line[1:]:
                    if c is not " ":
                        q.append(float(c))
            trajectory.append(np.array(q))
            
            line = tF.readline()
            
    return trajectory, N

"""
    Modified readTrajectory implementation
"""
def readTrajectorySkip(trajFile, skip, atom):
    """Does not output the first 'skip'-lines of the file, while only selecting atoms of type "atom".
    Works only .xyz with tab-delimiter (\t).
    input:
        trajFile= string of filename including xyz-extension
        skip=number of lines to skip
        atom= string of which atom to select
    """
    trajectory=[]
    with open(trajFile, "r") as tF:
        for _ in range(skip):
            next(tF)
            
        line = tF.readline()
        while line is not "":
            #first line is number of atoms
            N = int(line.strip())
            tF.readline().strip() # second line is a comment that we throw away

            q = []
            for i in range(N):            
                line = tF.readline().strip().split("\t") # this corresponds with utils.output_xyz
                if line[0]==atom: # selects only the given atom-type
                    for c in line[1:]:
                        if c is not " ":
                            q.append(float(c))
            trajectory.append(np.array(q))
            
            line = tF.readline()
            
    return trajectory, N

"""
    Return N element strings based on the N first elements read from the file
"""
def readElements(trajFile, N):
    with open(trajFile, "r") as tF:
        tF.readline().strip()
        tF.readline().strip()
        return [tF.readline().strip().split(" ")[0] for i in range(N)]


"""
Outputs an xyz-file of the trajectory (for homogeeous systems only)
INPUT:
    T,1-list of single n,k,3-arrays of the positions of the atoms, where
            T is the number of time steps,
            n is the number of molecules,
            k is number of atoms per molecules,
            3 stands for the xyz-coordinates of the position.
    a string representing the name of the file plus the extension, e.g. "name.xyz"
    k-list containing the first character of the name of the corresponding atom as a string, e.g. "O"
OUTPUT:
    an .xyz file of the trajactory of q, placed in the working directory.
"""  
def Output_xyz(q,filename,list_of_names):
    n_molecules=len(q[0][0])
    n_atoms=len(q[0][0][0])# counting number of atoms in a molecule
    with open(filename,"w") as file:
        for i in tqdm(range(len(q)), desc='Writing XYZ', ncols=80):
            file.write("{} \n".format(n_atoms*n_molecules))
            file.write("trajectory of {} molecules \n".format(n_molecules))
            Q=np.ndarray.flatten(np.around(np.array(q[i]),decimals=4))*10 # assuming length units are in nm, VMD assumes length in Angstrom
            for j in range(0,len(Q),3):
                file.write("{}\t \t {}\t \t {}\t \t {}\n".format(list_of_names[np.mod(int(j/3),n_atoms)],Q[j],Q[j+1],Q[j+2]))
    return

"""
input:
    q=T,2-list of ni,ki,3-arrays of the positions of the atoms, where
            T is the number of time steps,
            ni is the number of molecules of type i,
            ki is number of atoms per molecules of type i,
            3 stands for the xyz-coordinates of the position.
    filename = title of the filename, such include ".xyz" extension.
    list_of_names= [names of atoms of molecule 1, names of atoms of molecule 2]
"""

def Output_xyz2(q,filename,list_of_names):
    with open(filename,"w") as file:
        nMolecules1=len(q[0][0]) # counting number of molecules of type i
        nMolecules2=len(q[0][1])
        nAtoms=[len(q[0][0][0]),len(q[0][1][0])]# counting number of atoms in a molecule of type i
        for i in tqdm(range(len(q)), desc='Writing XYZ', ncols=80):
            file.write("{} \n".format(nAtoms[0]*nMolecules1+nAtoms[1]*nMolecules2))
            file.write("trajectory of {} molecules \n".format(nMolecules1*nMolecules2))
            for k in range(len(q[0])):
                Q=np.ndarray.flatten(np.around(np.array(q[i][k]),decimals=4))*10 # assuming length units are in nm, VMD assumes length in Angstrom
                for j in range(0,len(Q),3):
                    file.write("{}\t \t {}\t \t {}\t \t {}\n".format(list_of_names[k][np.mod(int(j/3),nAtoms[k])],Q[j],Q[j+1],Q[j+2]))
    return



