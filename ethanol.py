# -*- coding: utf-8 -*-
"""
Created on Wed Dec 19 12:16:43 2018

@author: bsmetsjr

Ethanol specific physics

"""
import numpy as np
import math
import geometry as g

from math import pi

# Distances in nm,
# angles in radians,
# epsilons in kJ/mol
# stiffness in kJ / (mol nm^2)

atomNames=['C', 'H', 'H', 'H', 'C', 'H', 'H', 'O', 'H']

# Boltzmann constant for kJ/(mol K)
kB = 0.0083144621


mH = 1.0080 # amu
mO = 15.9994 # amu
mC = 12.0110 # amu
m = np.array([mC, mH, mH, mH, mC, mH, mH, mO, mH])

# Bond parameters
Rb51 = 0.1529
Rb31 = Rb41 = Rb21 = Rb65 = Rb75 = 0.1090 
Rb85 = 0.1410
Rb98 = 0.0945

Kb51 = 224262.4
Kb31 = Kb41 = Kb21 = Kb65 = Kb75 = 284512.0
Kb85 = 267776.0
Kb98 = 462750.0

# Angle parameters
Ta215 = Ta315 = Ta415 = 108.5 * pi/180.0
Ta413 = Ta412 = Ta312 = Ta657 = 107.8 * pi/180.0
Ta157 = Ta156 = 110.7 * pi/180.0
Ta158 = 109.5 * pi/180.0
Ta589 = 108.5 * pi/180.0
Ta658 = Ta758 = 109.5  * pi/180.0

Ka215 = Ka315 = Ka415 = 292.880
Ka413 = Ka412 = Ka312 = Ka657 = 276.144
Ka157 = Ka156 = 313.800
Ka158 = 414.400
Ka589 = 460.240
Ka658 = Ka758 = 292.880


# Ryckaert-Bellmann dihedral parameters
C2156 = C3156 = C4156 = C2157 = C3157 = C4157 =[0.62760, 1.88280, 0.00000, -3.91622]
C2158 = C3158 = C4158 = [0.97905, 2.93716, 0.00000, -3.91622]
C1589 = [-0.44310, 3.83255, 0.72801, -4.11705]
C6589 = C7589 = [0.94140, 2.82420, 0.00000, -3.76560]



# Lennard-Jones parameters
LjSigmaHc, LjEpsilonHc = 0.25, 0.125520
LjSigmaHo, LjEpsilonHo = 0.0, 0.0
LjSigmaC, LjEpsilonC = 0.35, 0.276144
LjSigmaO, LjEpsilonO = 0.312, 0.711280


LjSigmas = np.array([LjSigmaC, LjSigmaHc, LjSigmaHc, LjSigmaHc, LjSigmaC, LjSigmaHc, LjSigmaHc, LjSigmaO, LjSigmaHo])
LjEpsilons = np.array([LjEpsilonC, LjEpsilonHc, LjEpsilonHc, LjEpsilonHc, LjEpsilonC, LjEpsilonHc, LjEpsilonHc, LjEpsilonO, LjEpsilonHo])

LjSigmaMix = 1/2*(LjSigmas[None,:]+LjSigmas[:,None])
LjEpsilonMix = np.sqrt(np.outer(LjEpsilons, LjEpsilons))




def createInitialQV2(boxsize, nMolecules, T):
    """
Initial q,v structure in a cube at the given temperature.
    boxsize: size of the cube (nm)
    nMolecules: number of molecules
    T: initial temperature (K)
    
    returns:
        q: numpy.array[nMolecules, 3, (x,y,z)]
        v: numpy.array[nMoleclues, 3, (vx,vy,vz)]
    """
    # generate equidistant grid
    gridsize = math.ceil(nMolecules**(1/3))
    gap = boxsize/gridsize
    s = np.arange(gap/2, boxsize, gap)
    x,y,z = [c.flatten()[0:nMolecules] for c in np.meshgrid(s, s, s)]
    
    ex = np.tile(np.array([1,0,0]), nMolecules).reshape(nMolecules,3)
    ey = np.tile(np.array([0,1,0]), nMolecules).reshape(nMolecules,3)
    ez = np.tile(np.array([0,0,1]), nMolecules).reshape(nMolecules,3)
    sq = np.sqrt(2)/2
    q1 = np.array([x,y,z]).transpose()
    q2 = q1 - Rb21*sq*ey - Rb21*sq*ez
    q3 = q1 - Rb31*sq*ex + Rb31*sq*ez
    q4 = q1 + Rb41*sq*ey - Rb41*sq*ez
    q5 = q1 + Rb51*ex
    q6 = q5 + Rb65*sq*ey + Rb65*sq*ez 
    q7 = q5 - Rb75*sq*ey + Rb75*sq*ez
    q8 = q5 + Rb85*sq*ex - Rb85*sq*ez
    q9 = q8 + Rb98*sq*ex + Rb98*sq*ey
    
    q = np.stack((q1, q2, q3, q4, q5, q6, q7, q8, q9), axis=1)
    
    # generate random velocity vectors and scale to temperature
    v = np.random.normal(loc=0.0, scale=1.0, size=q.shape)
    v = v*np.sqrt(T/temperature(v))
    
    
    return q,v

def createInitialQV(boxsize, nMolecules, T):
    """
        Initial q,v structure in a cube at the given temperature.
        boxsize: size of the cube (nm)
        nMolecules: number of molecules
        T: initial temperature (K)
    
        returns:
            q: numpy.array[nMolecules, 3, (x,y,z)]
            v: numpy.array[nMoleclues, 3, (vx,vy,vz)]
    """
    # generate equidistant grid
    gridsize = math.ceil(nMolecules**(1/3))
    gap = boxsize/gridsize
    s = np.arange(gap/2, boxsize, gap)
    x,y,z = [c.flatten()[0:nMolecules] for c in np.meshgrid(s, s, s)]
    
    RbCC = 0.1529
    RbCH = 0.1090
    RbOH = 0.0945
    RbCO = 0.141
    aCHH = 107.8 * math.pi/180
    aCHC= 108.5*math.pi/180
    aCCH = 110.7* math.pi/180
    aCCO= 109.5* math.pi/180
    
    # fix the two C-atoms with distance RbCC, and first H-atom connected to the first C.
    q0=np.array([x,y,z]).transpose()
    q4=q0 + np.array([RbCC,0,0])
    q1 = q0 + np.array([np.cos(-aCHC),np.sin(-aCHC),0])*RbCH
    
    q2= q0+ angledVector(np.array([np.cos(-aCHC),np.sin(-aCHC),0]),aCHC,aCHH)*RbCH
    q3=q0+ angledVector(np.array([np.cos(-aCHC),np.sin(-aCHC),0]),aCHC,-aCHH)*RbCH
    
    q5=q4+angledVector(np.array([np.cos(aCCH),np.sin(-aCCH),0]),np.pi-aCCH,2*aCHH/3)*RbCH # the angle 2*aCHH/3 is due to the geometry
    q6=q4+angledVector(np.array([np.cos(aCCH),np.sin(-aCCH),0]),np.pi-aCCH,-2*aCHH/3)*RbCH
    
    q7=q4+np.array([np.cos(np.pi-aCCO),np.sin(np.pi-aCCO),0])*RbCO  # educated guess of the equilibrium position of O-atom
    q8=q7+np.array([RbOH,0,0]) # educated guess
    
    q= np.stack((q0,q1,q2,q3,q4,q5,q6,q7,q8), axis=1)
    # generate random velocity vectors and scale to temperature
    v = np.random.normal(loc=0.0, scale=1.0, size=q.shape)
    v = v*np.sqrt(T/temperature(v))
    return q,v

def angledVector(x,theta,phi):
    """
    Used in createInitialQV. Given a vector x=[x0,x1,0], and two angles theta and phi
    returns unit vector with x-axis angle theta, and angle phi w.r.t vector x.
    """
    qx=np.cos(theta)
    qy=(np.cos(phi)-np.cos(theta)*x[0])/x[1]
    qz=np.sqrt(np.sin(theta)**2-(np.cos(phi)-np.cos(theta)*x[0])**2/x[1]**2)*np.sign(phi)
    return np.array([qx,qy,qz])


def avgK(v):
    """
Calculate average kinetic energy
    """
    return 1e6*np.average(np.sum(v*v, axis=-1)*m/2)


def temperature(v):
    """
Calculate average temperature
    """
    return 2/3 * avgK(v) /kB

    
def isokineticThermostat(q, v, T):
    """
Isokinetic thermostat, scale the velocities to obtain temperature T.
    """
    return q, v*np.sqrt(T/temperature(v))



def accelerationFunction2(qs, vs, boxsize):
    """
Calculate acceleration for the given state qs, vs (only for ethanol).
    """
    q = qs[0]
    v = vs[0]
    
    dri = g.periodicRelativePositionTensor(q, boxsize)
    disti = g.distanceTensor(dri)
    dr = g.periodicRelativePositionTensor(
            q.reshape(q.shape[0]*q.shape[1], q.shape[2]), boxsize)
    dist = g.distanceTensor(dr)
    
    # we zero out the distance between atoms in the same molecule to make LJ calculation easier
    indices = np.arange(dist.shape[0])
    grid = np.dstack(np.meshgrid(indices, indices))
    mol_id = np.floor_divide(grid[...,0], q.shape[1]) == np.floor_divide(grid[...,1], q.shape[1])
    dist[mol_id] = 0
    
    
    Fin = np.zeros_like(q)
    
    # Bond forces
    fb21 = g.bondForce(dri, disti, 0, 1, [Rb21, Kb21])
    Fin[:,(0,1),:] += fb21
    
    fb31 = g.bondForce(dri, disti, 0, 2, [Rb31, Kb31])
    Fin[:,(0,2),:] += fb31
    
    fb41 = g.bondForce(dri, disti, 0, 3, [Rb41, Kb41])
    Fin[:,(0,3),:] += fb41
    
    fb51 = g.bondForce(dri, disti, 0, 4, [Rb51, Kb51])
    Fin[:,(0,4),:] += fb51
    
    fb65 = g.bondForce(dri, disti, 4, 5, [Rb65, Kb65])
    Fin[:,(4,5),:] += fb65
    
    fb75 = g.bondForce(dri, disti, 4, 6, [Rb75, Kb75])
    Fin[:,(4,6),:] += fb75
    
    fb85 = g.bondForce(dri, disti, 4, 7, [Rb85, Kb85])
    Fin[:,(4,7),:] += fb85
    
    fb98 = g.bondForce(dri, disti, 7, 8, [Rb98, Kb98])
    Fin[:,(7,8),:] += fb98
    
    # Angle forces
    fa215 = g.angleForce(dri, disti, 0, 1, 4, [Ta215,Ka215])
    Fin[:,(0,1,4),:] += fa215
    
    fa315 = g.angleForce(dri, disti, 0, 2, 4, [Ta315,Ka315])
    Fin[:,(0,2,4),:] += fa315
    
    fa415 = g.angleForce(dri, disti, 0, 3, 4, [Ta415,Ka415])
    Fin[:,(0,3,4),:] += fa415
    
    fa413 = g.angleForce(dri, disti, 0, 3, 2, [Ta413,Ka413])
    Fin[:,(0,3,2),:] += fa413
    
    fa412 = g.angleForce(dri, disti, 0, 3, 1, [Ta412,Ka412])
    Fin[:,(0,3,1),:] += fa412
    
    fa312 = g.angleForce(dri, disti, 0, 2, 1, [Ta312,Ka312])
    Fin[:,(0,2,1),:] += fa312
    
    fa657 = g.angleForce(dri, disti, 4, 5, 6, [Ta657,Ka657])
    Fin[:,(4,5,6),:] += fa657
    
    fa157 = g.angleForce(dri, disti, 4, 0, 6, [Ta157,Ka157])
    Fin[:,(4,0,6),:] += fa157
    
    fa156 = g.angleForce(dri, disti, 4, 0, 5, [Ta156,Ka156])
    Fin[:,(4,0,5),:] += fa156
    
    fa158 = g.angleForce(dri, disti, 4, 0, 7, [Ta158,Ka158])
    Fin[:,(4,0,7),:] += fa158
    
    fa589 = g.angleForce(dri, disti, 7, 4, 8, [Ta589,Ka589])
    Fin[:,(7,4,8),:] += fa589
    
    fa658 = g.angleForce(dri, disti, 4, 5, 7, [Ta658,Ka658])
    Fin[:,(4,5,7),:] += fa658
    
    fa758 = g.angleForce(dri, disti, 4, 6, 7, [Ta758,Ka758])
    Fin[:,(4,6,7),:] += fa758
    
    # Dihedral forces
    fd2156 = g.dihedralForceRb(dri, disti, 1, 0, 4, 5, C2156)
    Fin[:,(1,0,4,5),:] += fd2156
    
    fd3156 = g.dihedralForceRb(dri, disti, 2, 0, 4, 5, C3156)
    Fin[:,(2,0,4,5),:] += fd3156
    
    fd4156 = g.dihedralForceRb(dri, disti, 3, 0, 4, 5, C4156)
    Fin[:,(3,0,4,5),:] += fd4156
    
    fd2157 = g.dihedralForceRb(dri, disti, 1, 0, 4, 6, C2157)
    Fin[:,(1,0,4,6),:] += fd2157
    
    fd3157 = g.dihedralForceRb(dri, disti, 2, 0, 4, 6, C3157)
    Fin[:,(2,0,4,6),:] += fd3157
    
    fd4157 = g.dihedralForceRb(dri, disti, 3, 0, 4, 6, C4157)
    Fin[:,(3,0,4,6),:] += fd4157
    
    fd2158 = g.dihedralForceRb(dri, disti, 1, 0, 4, 7, C2158)
    Fin[:,(1,0,4,7),:] += fd2158
    
    fd3158 = g.dihedralForceRb(dri, disti, 2, 0, 4, 7, C3158)
    Fin[:,(2,0,4,7),:] += fd3158
    
    fd4158 = g.dihedralForceRb(dri, disti, 3, 0, 4, 7, C4158)
    Fin[:,(3,0,4,7),:] += fd4158
    
    fd1589 = g.dihedralForceRb(dri, disti, 0, 4, 7, 8, C1589)
    Fin[:,(0,4,7,8),:] += fd1589
    
    fd6589 = g.dihedralForceRb(dri, disti, 5, 4, 7, 8, C6589)
    Fin[:,(5,4,7,8),:] += fd6589
    
    fd7589 = g.dihedralForceRb(dri, disti, 6, 4, 7, 8, C7589)
    Fin[:,(6,4,7,8),:] += fd7589
    
    #build LJ parameter matrices A and B
    A = 4*LjEpsilonMix*LjSigmaMix**12
    B = 4*LjEpsilonMix*LjSigmaMix**6
    A = np.tile(A, (q.shape[0], q.shape[0]))
    B = np.tile(B, (q.shape[0], q.shape[0]))
    
    Flj = g.ljForce(dr, dist, A, B)
    Flj = np.sum(Flj, axis=1).reshape(q.shape)
    # alternative way to compute Flj
    """
    Flj=g.monoLjForce2(q,[LjSigmaO,0,0],[LjEpsilonO,0,0])
    """ 
    A = (1e-6)*(Fin+Flj)/m[:,None] # convert kJ/(g*nm) to nm/fs^2
    return [A]


def potentialEnergy(qs, vs, boxsize):
    """
Calculate the potential energy for the given state qs, vs (only for ethanol).
Returns the potential energy per molecule in kJ/mol.
    """
    q = qs[0]
    v = vs[0]
    
    dri = g.periodicRelativePositionTensor(q, boxsize)
    disti = g.distanceTensor(dri)
    dr = g.periodicRelativePositionTensor(
            q.reshape(q.shape[0]*q.shape[1], q.shape[2]), boxsize)
    dist = g.distanceTensor(dr)
    
    # we zero out the distance between atoms in the same molecule to make LJ calculation easier
    indices = np.arange(dist.shape[0])
    grid = np.dstack(np.meshgrid(indices, indices))
    mol_id = np.floor_divide(grid[...,0], q.shape[1]) == np.floor_divide(grid[...,1], q.shape[1])
    dist[mol_id] = 0
    
    
    Vin = np.zeros(q.shape[0])
    
    # Bond forces
    Vb21 = g.bondPotential(dri, disti, 0, 1, [Rb21, Kb21])
    Vin += Vb21
    
    Vb31 = g.bondPotential(dri, disti, 0, 2, [Rb31, Kb31])
    Vin += Vb31
    
    Vb41 = g.bondPotential(dri, disti, 0, 3, [Rb41, Kb41])
    Vin += Vb41
    
    Vb51 = g.bondPotential(dri, disti, 0, 4, [Rb51, Kb51])
    Vin += Vb51
    
    Vb65 = g.bondPotential(dri, disti, 4, 5, [Rb65, Kb65])
    Vin += Vb65
    
    Vb75 = g.bondPotential(dri, disti, 4, 6, [Rb75, Kb75])
    Vin += Vb75
    
    Vb85 = g.bondPotential(dri, disti, 4, 7, [Rb85, Kb85])
    Vin += Vb85
    
    Vb98 = g.bondPotential(dri, disti, 7, 8, [Rb98, Kb98])
    Vin += Vb98
    
    # Angle forces
    Va215 = g.anglePotential(dri, disti, 0, 1, 4, [Ta215,Ka215])
    Vin += Va215
    
    Va315 = g.anglePotential(dri, disti, 0, 2, 4, [Ta315,Ka315])
    Vin += Va315
    
    Va415 = g.anglePotential(dri, disti, 0, 3, 4, [Ta415,Ka415])
    Vin += Va415
    
    Va413 = g.anglePotential(dri, disti, 0, 3, 2, [Ta413,Ka413])
    Vin += Va413
    
    Va412 = g.anglePotential(dri, disti, 0, 3, 1, [Ta412,Ka412])
    Vin += Va412
    
    Va312 = g.anglePotential(dri, disti, 0, 2, 1, [Ta312,Ka312])
    Vin += Va312
    
    Va657 = g.anglePotential(dri, disti, 4, 5, 6, [Ta657,Ka657])
    Vin += Va657
    
    Va157 = g.anglePotential(dri, disti, 4, 0, 6, [Ta157,Ka157])
    Vin += Va157
    
    Va156 = g.anglePotential(dri, disti, 4, 0, 5, [Ta156,Ka156])
    Vin += Va156
    
    Va158 = g.anglePotential(dri, disti, 4, 0, 7, [Ta158,Ka158])
    Vin += Va158
    
    Va589 = g.anglePotential(dri, disti, 7, 4, 8, [Ta589,Ka589])
    Vin += Va589
    
    Va658 = g.anglePotential(dri, disti, 4, 5, 7, [Ta658,Ka658])
    Vin += Va658
    
    Va758 = g.anglePotential(dri, disti, 4, 6, 7, [Ta758,Ka758])
    Vin += Va758
    
    # Dihedral forces
    Vd2156 = g.dihedralPotentialRb(dri, disti, 1, 0, 4, 5, C2156)
    Vin += Vd2156
    
    Vd3156 = g.dihedralPotentialRb(dri, disti, 2, 0, 4, 5, C3156)
    Vin += Vd3156
    
    Vd4156 = g.dihedralPotentialRb(dri, disti, 3, 0, 4, 5, C4156)
    Vin += Vd4156
    
    Vd2157 = g.dihedralPotentialRb(dri, disti, 1, 0, 4, 6, C2157)
    Vin += Vd2157
    
    Vd3157 = g.dihedralPotentialRb(dri, disti, 2, 0, 4, 6, C3157)
    Vin += Vd3157
    
    Vd4157 = g.dihedralPotentialRb(dri, disti, 3, 0, 4, 6, C4157)
    Vin += Vd4157
    
    Vd2158 = g.dihedralPotentialRb(dri, disti, 1, 0, 4, 7, C2158)
    Vin += Vd2158
    
    Vd3158 = g.dihedralPotentialRb(dri, disti, 2, 0, 4, 7, C3158)
    Vin += Vd3158
    
    Vd4158 = g.dihedralPotentialRb(dri, disti, 3, 0, 4, 7, C4158)
    Vin += Vd4158
    
    Vd1589 = g.dihedralPotentialRb(dri, disti, 0, 4, 7, 8, C1589)
    Vin += Vd1589
    
    Vd6589 = g.dihedralPotentialRb(dri, disti, 5, 4, 7, 8, C6589)
    Vin += Vd6589
    
    Vd7589 = g.dihedralPotentialRb(dri, disti, 6, 4, 7, 8, C7589)
    Vin += Vd7589
    
    #build LJ parameter matrices A and B
    A = 4*LjEpsilonMix*LjSigmaMix**12
    B = 4*LjEpsilonMix*LjSigmaMix**6
    A = np.tile(A, (q.shape[0], q.shape[0]))
    B = np.tile(B, (q.shape[0], q.shape[0]))
    
    Vlj = np.sum(g.ljPotential(dr, dist, A, B), axis=-1)
    Vlj = np.sum(Vlj.reshape(q.shape[:-1]), axis=-1)
    

    return Vin+Vlj




def accelerationFunction(qs,vs,boxsize):
    m=np.array([12.011,1.008,1.008,1.008,12.011,1.008,1.008,16,1.008]) # masses in amu
    
    constantsBonded=[[0.109,284512],[0.109,284512],[0.109,284512],[0.1529,224262.4],
                     [0.109,284512],[0.109,284512],[0.1410,267776],[0.0945,462750]] # list of [r_b,K_b]
    indicesBonded=[[0,1,0], [0,2,1],[0,3,2],[0,4,3],[4,5,4],[4,6,5],[4,7,6],[7,8,7]] # list of [1st atom, 2nd atom, index w.r.t. constantBonded]
    
    constantsAngle=[[107.8, 276.144],[107.8,276.144],[107.8,276.144],[108.5,292.880],[108.5,292.880],
                    [108,292.880],[110.7,313.8],[110.7,313.8],
                    [107.8,276.144],[109.5,292.88],[109.5,292.88],[108.5,460.24],[109.5,414.4]] # list of [r_a, K_a]
    indicesAngle=[[0,1,2,0],[0,1,3,1],[0,2,3,2],[0,1,4,3],[0,2,4,4],[0,3,4,5],
                  [4,0,5,6],[4,0,6,7],[4,5,6,8],[4,5,7,9],[4,6,7,10],[4,7,8,11], [4,0,7,12]] #list of[central atom, branch atom, branch atom, index w.r.t. constantAngle]
   
    constantsDihedral=[[0.62760,1.8828,0,-3.91622],[0.62760,1.8828,0,-3.91622],[0.62760,1.8828,0,-3.91622],
                       [0.62760,1.8828,0,-3.91622],[0.62760,1.8828,0,-3.91622],[0.62760,1.8828,0,-3.91622],
                        [0.97905,2.93716,0,-3.91622],[0.97905,2.93716,0,-3.91622],[0.97905,2.93716,0,-3.91622],
                        [-0.44310,3.83255,0.72801,-4.11705],
                        [0.94140,2.82420,0,-3.7656],[0.94140,2.82420,0,-3.7656]] # list of [C1, C2, C3, C4]
    indicesDihedral=[[1,0,4,5,0],[2,0,4,5,1],[3,0,4,5,2],[1,0,4,6,3],[2,0,4,6,4],[3,0,4,6,5],
                     [1,0,4,7,6],[2,0,4,7,7],[3,0,4,7,8],[0,4,7,8,9],[5,4,7,8,10],[6,4,7,8,11]]# outmost atoms of the chain must be located at index 0 and -1 (well defined, since we only consider proper dihedrals)
    sigmaLJ=[0.35,0.25,0.25,0.25,0.35,0.25,0.25,0.312,0]
    epsilonLJ=[0.276144,0.12552,0.12552,0.12552,276144,0.12552,0.12552,0.71128,0]
    
    q=qs[0]
    v=vs[0]
    dri = g.periodicRelativePositionTensor(q, boxsize)
    disti = g.distanceTensor(dri)
    
    Fin = np.zeros_like(q)
    for index in indicesBonded:
        fb = g.bondForce(dri, disti, index[0], index[1], constantsBonded[index[2]])
        Fin[:,(index[0],index[1]),:] += fb
    for index in indicesAngle:
        fa=g.angleForce(dri,disti,index[0],index[1],index[2],constantsAngle[index[3]])
        Fin[:,(index[0],index[1],index[2]),:] += fa
    for index in indicesDihedral:
        fdih=g.dihedralForce(dri,index[0],index[1],index[2],index[3],constantsDihedral[index[4]])# g.
        Fin[:,(index[0],index[1],index[2],index[3]),:]+= fdih
    fLJ=g.monoLjForce2(q,sigmaLJ,epsilonLJ) #g.
    A = (Fin + fLJ)/m[:,np.newaxis]
    return [A]
    










"""
# test ethanol molecule (no consideration of realistic angles), C1,H2,H3,H4,C5,H6,H7,O8,H9
ethanol=np.array([[0,0,0],[-0.05,-0.05,-0.01],[-0.08,0,0.02],[-0.01,0.08,0.01],[0.15,0,0],[0.15,-0.1,-0.01],[0.12,0.08,0],[0.14,0.01,-0.02],[0.1605,0.005,0]]) # may not be nice for dihedral angles
m=np.array([12.011,1.008,1.008,1.008,12.011,1.008,1.008,16,1.008]) # in amu
q_0=[np.array([ethanol,ethanol*1.3+1])]# initial positions of ethanol molecules
v=[[0,0.1,0],[0.1,0,0],[0,0,-0.1],[0.1,0,0],[0,0.1,0],[0.1,0,-0.01],[0,0,0.1],[-0.1,0,0],[0.1,0,0]]
v_0=[np.array([v,v])] # zero initial velocity
"""

"""
TEST: testing the effect of the dihedral force on H-C-C-H molecule. First, constructing molecule with a known
    dihedral angle and equilibrium angle, then computing tailered accelFunction.
"""


"""
import integrators
import utils
test_molecule=np.array([[0,1,0],[0,0,0],[1,0,0],[1,0.5,0.5]])

def accelerationFunction_T(q,v):
    m=np.array([1,12,12,1])
    constants=[[0.62760,1.8828,0,-3.91622]]
    indices=[[0,1,2,3,0]]
    a=[]
    for molecule in q[0]:
        dr=gm.relativePositionTensor(molecule)
        F_molecule=np.zeros((4,3))
        for index in indices:
            f=Force_dihedral3(dr[index[:4]][:,index[:4]],constants[index[4]])
            F_molecule[index[0]]=f[0]
            F_molecule[index[1]]=f[1]
            F_molecule[index[2]]=f[2]
            F_molecule[index[3]]=f[3]
        a.append(F_molecule/m[:,np.newaxis])
    return [np.array(a)*10**(-3)]

dt=0.02
t_end=20

q_0=[np.array([test_molecule/10, test_molecule/10 +0.2])]
v_0=[np.zeros([1,4,3])]

q_VelocityVerlet=[q_0]
v_VelocityVerlet=[v_0]
for i in range(int(t_end/dt)):
    Q,V=integrators.velocityVerlet(q_VelocityVerlet[i],v_VelocityVerlet[i],accelerationFunction_T,dt) #ethanol.
    q_VelocityVerlet.append(Q)
    v_VelocityVerlet.append(V)    

utils.Output_xyz(q_VelocityVerlet,"Trajectory of H-C-C-H molecule.xyz",["H","C","C","H"])
"""

