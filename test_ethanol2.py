# -*- coding: utf-8 -*-
"""
Created on Fri Feb  1 19:12:55 2019

@author: bsmetsjr
"""
import numpy as np

import integrators
import ethanol
import utils

from tqdm import tqdm # cool progress bar

boxsize = 3.0 #nm
nEthanol = int(10*boxsize**3)
q,v = ethanol.createInitialQV(boxsize, nEthanol, 300)
qs = [q]
vs = [v]

dt = 1.0
t_end = 1.0e4
qst = [qs]
vst = [vs]

for i in tqdm(range(int(t_end/dt)), desc='Simulating ', ncols=80):
    qs, vs = integrators.velocityVerlet(qs, vs, ethanol.accelerationFunction2, dt, boxsize)
    qst.append(qs)
    vst.append(vs)


utils.Output_xyz(qst, 'test_ethanol.xyz', ['C','H','H','H','C','H','H','O','H'])
print('Done, results in test_ethanol.xyz')

