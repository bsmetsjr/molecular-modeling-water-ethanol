# -*- coding: utf-8 -*-
"""
Created on Wed Dec  5 11:10:11 2018

@author: bsmetsjr

Water-specific physics

"""
import numpy as np
import geometry as g
import math
from math import pi

atomNames=['O','H','H']

Kbond = 1/256.
Kangle = 1/512.
EquiAngle = 1.85
EquiLength = 1.

# Distances in nm,
# angles in radians,
# epsilons in kJ/mol
# stiffness in kJ / (mol nm^2)


# Boltzmann constant for kJ/(mol K)
kB = 0.0083144621

# Lennard-Jones Parameters
LjSigmaO = 0.315061
LjSigmaH = 0.0

LjEpsilonO = 0.66386
LjEpsilonH = 0.0

LjSigmas = np.array([LjSigmaO, LjSigmaH, LjSigmaH])
LjEpsilons = np.array([LjEpsilonO, LjEpsilonH, LjEpsilonH])

LjSigmaMix = 1/2*(LjSigmas[None,:]+LjSigmas[:,None])
LjEpsilonMix = np.sqrt(np.outer(LjEpsilons, LjEpsilons))


# Bond parameters
Rb = 0.09572
Kb = 502416

# Angle parameters
Ta = 104.52 * pi/180.0
Ka = 628.02

# weights
mH = 1.0080 # amu
mO = 15.9994 # amu
m = np.array([mO, mH, mH]) # g/mol



def createInitialQV(boxsize, nMolecules, T):
    """
Initial q,v structure in a cube at the given temperature.
    boxsize: size of the cube (nm)
    nMolecules: number of molecules
    T: initial temperature (K)
    
    returns:
        q: numpy.array[nMolecules, 3, (x,y,z)]
        v: numpy.array[nMoleclues, 3, (vx,vy,vz)]
    """
    # generate equidistant grid
    gridsize = math.ceil(nMolecules**(1/3))
    gap = boxsize/gridsize
    s = np.arange(gap/2, boxsize, gap)
    x,y,z = [c.flatten()[0:nMolecules] for c in np.meshgrid(s, s, s)]
    
    # fix oxygen at the grid-points and offset the hydrogens
    qO = np.array([x,y,z]).transpose()
    qH1 = qO - np.array([math.cos(pi/2-Ta/2)*Rb, math.sin(pi/2-Ta/2)*Rb, 0])
    qH2 = qO - np.array([-math.cos(pi/2-Ta/2)*Rb, math.sin(pi/2-Ta/2)*Rb, 0])
    q = np.stack((qO, qH1, qH2), axis=1)
    
    # generate random velocity vectors and scale to temperature
    v = np.random.normal(loc=0.0, scale=1.0, size=q.shape)
    q,v = isokineticThermostat(q, v, T)
    
    
    return q,v
    


def accelerationFunction(qs, vs, boxsize):
    """
Calculate acceleration for the given state qs, vs (only for water).
    """
    q = qs[0]
    v = vs[0]
    
    dri = g.periodicRelativePositionTensor(q, boxsize)
    disti = g.distanceTensor(dri)
    dr = g.periodicRelativePositionTensor(
            q.reshape(q.shape[0]*q.shape[1], q.shape[2]), boxsize)
    dist = g.distanceTensor(dr)
    
    # we zero out the distance between atoms in the same molecule to make LJ calculation easier
    indices = np.arange(dist.shape[0])
    grid = np.dstack(np.meshgrid(indices, indices))
    mol_id = np.floor_divide(grid[...,0], q.shape[1]) == np.floor_divide(grid[...,1], q.shape[1])
    dist[mol_id] = 0
    
    
    Fin = np.zeros_like(q)
    
    fb1 = g.bondForce(dri, disti, 0, 1, [Rb, Kb])
    Fin[:,(0,1),:] += fb1
    
    fb2 = g.bondForce(dri, disti, 0, 2, [Rb, Kb])
    Fin[:,(0,2),:] += fb2
    
    fa = g.angleForce(dri, disti, 0, 1, 2, [Ta,Ka])
    Fin[:,(0,1,2),:] += fa
    
    #build LJ parameter matrices A and B
    A = 4*np.array([[LjEpsilonO*LjSigmaO**12,0,0],[0,0,0],[0,0,0]])
    B = 4*np.array([[LjEpsilonO*LjSigmaO**6,0,0],[0,0,0],[0,0,0]])
    A = np.tile(A, (q.shape[0], q.shape[0]))
    B = np.tile(B, (q.shape[0], q.shape[0]))
    
    Flj = g.ljForce(dr, dist, A, B)
    Flj = np.sum(Flj, axis=1).reshape(q.shape)
    # alternative way to compute Flj
    """
    Flj=g.monoLjForce2(q,[LjSigmaO,0,0],[LjEpsilonO,0,0])
    """ 
    A = (1e-6)*(Fin+Flj)/m  #convert m/s^2 to nm/ns^2
    return [A]
    
    

def potentialEnergy(qs, vs, boxsize):
    """
Calculate potential energy for the given state qs, vs (only for water).
Returns the potential energy per molecule in kJ/mol.
    """
    q = qs[0]
    v = vs[0]
    
    dri = g.periodicRelativePositionTensor(q, boxsize)
    disti = g.distanceTensor(dri)
    dr = g.periodicRelativePositionTensor(
            q.reshape(q.shape[0]*q.shape[1], q.shape[2]), boxsize)
    dist = g.distanceTensor(dr)
    
    # we zero out the distance between atoms in the same molecule to make LJ calculation easier
    indices = np.arange(dist.shape[0])
    grid = np.dstack(np.meshgrid(indices, indices))
    mol_id = np.floor_divide(grid[...,0], q.shape[1]) == np.floor_divide(grid[...,1], q.shape[1])
    dist[mol_id] = 0
    
    
    Vin = np.zeros(q.shape[0])
    
    Vb1 = g.bondPotential(dri, disti, 0, 1, [Rb, Kb])
    Vin += Vb1
    
    Vb2 = g.bondPotential(dri, disti, 0, 2, [Rb, Kb])
    Vin += Vb2
    
    Va = g.anglePotential(dri, disti, 0, 1, 2, [Ta,Ka])
    Vin += Va
    
    #build LJ parameter matrices A and B
    A = 4*np.array([[LjEpsilonO*LjSigmaO**12,0,0],[0,0,0],[0,0,0]])
    B = 4*np.array([[LjEpsilonO*LjSigmaO**6,0,0],[0,0,0],[0,0,0]])
    A = np.tile(A, (q.shape[0], q.shape[0]))
    B = np.tile(B, (q.shape[0], q.shape[0]))
    
    Vlj = np.sum(g.ljPotential(dr, dist, A, B), axis=-1)
    Vlj = np.sum(Vlj.reshape(q.shape[:-1]), axis=-1)
    
    return Vin+Vlj





def avgK(v):
    """
Calculate average kinetic energy
    """
    return 1e6*np.average(np.sum(v*v, axis=-1)*m/2)


def temperature(v):
    """
Calculate average temperature
    """
    return 2/3 * avgK(v) /kB


    
def isokineticThermostat(q, v, T):
    """
Isokinetic thermostat, scale the velocities to obtain temperature T.
    """
    return q, v*np.sqrt(T/temperature(v))









