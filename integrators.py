# -*- coding: utf-8 -*-
"""
Created on Wed Nov 28 11:45:50 2018

@author: bsmetsjr

This file contains the following time integrators:
    1) Euler
    2) Verlet
    3) Leapfrog
    4) Velocity Verlet
    5) 3rd order symplectic
"""

import numpy as np
import geometry as g



def euler(q, v, accelerationFunction, dt):
        """
    Euler integrator
    
    inputs:
        q: list per species of np.array[..., #atom, (x,y,z)]
        v: list per species np.array[..., #atom, (vx,vy,vz)]
        
        accelerationFunction: function that takes (q,v) as argument and returns:
            list per species np.array[..., #atom, (ax,ay,az)]
            
        dt: float (timestep)
        
    return:
        (q_next, v_next)
        """
        assert len(q)==len(v)
        a = accelerationFunction(q,v)
        qnew = [q[i]+dt*v[i] + (dt**2)/2*a[i] for i in range(len(q))]
        vnew = [v[i]+dt*a[i] for i in range(len(q))]
        return qnew, vnew
   
    
    

def verlet(q_previous, q_current, accelerationFunction, dt):
    """
    Verlet integrator
    
    inputs:
        q_previous: list per species of np.array[..., #atom, (x,y,z)]
        q_current: list per species of np.array[..., #atom, (x,y,z)]
        
        accelerationFunction: function that takes (q,v) as argument and returns:
            a list per species of np.array[..., #atom, (ax,ay,az)]
            
        dt: float (timestep)
        
    return:
        q_next
    """
    assert len(q_previous)==len(q_current)
    a = accelerationFunction(q_current,None)
    qnew = [2*q_current[i] - q_previous[i] + (dt**2)/2*a[i] for i in range(len(q_current))] 
    return qnew, None





def leapfrog(q, v, accelerationFunction, dt):
    """
    Leapfrog integrator
    
    inputs:
        q: python list per species of np.array[..., #atom, (x,y,z)]
        v: python list per species of np.array[..., #atom, (vx,vy,vz)]
        
        accelerationFunction: function that takes (q,v) as argument and returns:
            python list per species of np.array[..., #atom, (ax,ay,az)]
            
        dt: float (timestep)
        
    return:
        (q_next, v_next)
    """
    assert len(q)==len(v)
    a = accelerationFunction(q, v)
    vnew = [v[i]+dt*a[i] for i in range(len(q))]
    qnew = [q[i]+dt*vnew[i] for i in range(len(q))]
    return qnew, vnew




def velocityVerlet(qs, vs, accelerationFunction, dt, boxsize):
    """
    Velocity Verlet
    
    inputs:
        qs: python list per species of np.array[..., #atom, (x,y,z)]
        vs: python list per species of np.array[..., #atom, (vx,vy,vz)]
        
        accelerationFunction: function that takes (q,v) as argument and returns:
            python list per species np.array[..., #atom, (ax,ay,az)]
            
        dt: float (timestep)
        boxsize: size of the periodic cell
    return:
        (qs_next, vs_next)
    """
    assert len(qs)==len(vs)
    a = accelerationFunction(qs, vs, boxsize)
    qsnew = [g.clampToBox(qs[i] + dt*vs[i] + (dt**2)/2*a[i], boxsize) for i in range(len(qs))]
    
    anew = accelerationFunction(qsnew, vs, boxsize)
    vsnew = [vs[i] + dt/2*(a[i]+anew[i]) for i in range(len(qs))]
    
    return qsnew, vsnew


def symplectic3(qs, vs, accelerationFunction, dt, boxsize):
    """
    3rd order symplectic, 
    see Ronald D. Ruth 1983, A Canonical Integration Technique.
    
    inputs:
        qs: python list per species of np.array[..., #atom, (x,y,z)]
        vs: python list per species of np.array[..., #atom, (vx,vy,vz)]
        
        accelerationFunction: function that takes (q,v) as argument and returns:
            python list per species np.array[..., #atom, (ax,ay,az)]
            
        dt: float (timestep)
        boxsize: size of the periodic cell
    return:
        (qs_next, vs_next)
    """
    assert len(qs)==len(vs)
    I = range(len(qs))
    c1, c2, c3 = 7.0/24.0, 3.0/4.0, -1.0/24.0
    d1, d2, d3 = 2.0/3.0, -2.0/3.0, 1.0
    
    a1 = accelerationFunction(qs, vs, boxsize)
    vs1 = [vs[i]+c1*dt*a1[i] for i in I]
    qs1 = [qs[i]+d1*dt*vs1[i] for i in I]
    
    a2 = accelerationFunction(qs1, vs1, boxsize)
    vs2 = [vs1[i]+c2*dt*a2[i] for i in I]
    qs2 = [qs1[i]+d2*dt*vs2[i] for i in I]
    
    a3 = accelerationFunction(qs2, vs2, boxsize)
    vs3 = [vs2[i]+c3*dt*a3[i] for i in I]
    qs3 = [qs2[i]+d3*dt*vs3[i] for i in I]
    
    return qs3, vs3
    
    











