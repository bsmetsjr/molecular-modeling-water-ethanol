# -*- coding: utf-8 -*-
"""
Created on Fri Feb  1 13:06:21 2019

@author: s149259
Testing the trajectory of ethanol molecules when only LJ-forces are applied.
"""

import numpy as np

import integrators
import ethanol
import utils
import geometry as g
from tqdm import tqdm # cool progress bar

boxsize = 1.0 # in nm

q,v = ethanol.createInitialQV(boxsize, 2, 300)
qs = [q]
vs = [v] 
#vs=[np.zeros_like(v)]

dt = 1.0e-3 # in ps
t_end = 1.0
qst = [qs]
vst = [vs]

for i in tqdm(range(int(t_end/dt)), desc='Simulating ', ncols=80):
    qs, vs = integrators.velocityVerlet(qs, vs, accelerationFunction_T, dt, boxsize)
    qst.append(qs)
    vst.append(vs)

utils.Output_xyz(qst, 'test_ethanol.xyz', ethanol.atomNames)





"""
TEMPORARY: Some code to analyze stability problem of ethanol simulation
"""
import matplotlib.pyplot as plt 
track=np.array([np.linalg.norm(qst[t][0][0][4]-qst[t][0][0][7]) for t in range(100)])
trackv=np.array([np.linalg.norm(vst[t][0][0][4]-vst[t][0][0][7]) for t in range(100)])
plt.plot(track)
plt.plot(trackv)

def accelerationFunction_T(qs,vs,boxsize):
    m=np.array([12.011,1.008,1.008,1.008,12.011,1.008,1.008,16,1.008]) # masses in amu
    
    constantsBonded=[[0.109,284512],[0.109,284512],[0.109,284512],[0.1529,224262.4],
                     [0.109,284512],[0.109,284512],[0.1410,267776],[0.0945,462750]] # list of [r_b,K_b]
    indicesBonded=[[0,1,0], [0,2,1],[0,3,2],[0,4,3],[4,5,4],[4,6,5],[4,7,6],[7,8,7]] # list of [1st atom, 2nd atom, index w.r.t. constantBonded]
    
    constantsAngle=[[107.8, 276.144],[107.8,276.144],[107.8,276.144],[108.5,292.880],[108.5,292.880],
                    [108,292.880],[110.7,313.8],[110.7,313.8],
                    [107.8,276.144],[109.5,292.88],[109.5,292.88],[108.5,460.24],[109.5,414.4]] # list of [r_a, K_a]
    indicesAngle=[[0,1,2,0],[0,1,3,1],[0,2,3,2],[0,1,4,3],[0,2,4,4],[0,3,4,5],
                  [4,0,5,6],[4,0,6,7],[4,5,6,8],[4,5,7,9],[4,6,7,10],[4,7,8,11], [4,0,7,12]] #list of[central atom, branch atom, branch atom, index w.r.t. constantAngle]
   
    constantsDihedral=[[0.62760,1.8828,0,-3.91622],[0.62760,1.8828,0,-3.91622],[0.62760,1.8828,0,-3.91622],
                       [0.62760,1.8828,0,-3.91622],[0.62760,1.8828,0,-3.91622],[0.62760,1.8828,0,-3.91622],
                        [0.97905,2.93716,0,-3.91622],[0.97905,2.93716,0,-3.91622],[0.97905,2.93716,0,-3.91622],
                        [-0.44310,3.83255,0.72801,-4.11705],
                        [0.94140,2.82420,0,-3.7656],[0.94140,2.82420,0,-3.7656]] # list of [C1, C2, C3, C4]
    indicesDihedral=[[1,0,4,5,0],[2,0,4,5,1],[3,0,4,5,2],[1,0,4,6,3],[2,0,4,6,4],[3,0,4,6,5],
                     [1,0,4,7,6],[2,0,4,7,7],[3,0,4,7,8],[0,4,7,8,9],[5,4,7,8,10],[6,4,7,8,11]]# outmost atoms of the chain must be located at index 0 and -1 (well defined, since we only consider proper dihedrals)
    sigmaLJ=[0.35,0.25,0.25,0.25,0.35,0.25,0.25,0.312,0]
    epsilonLJ=[0.276144,0.12552,0.12552,0.12552,0.276144,0.12552,0.12552,0.71128,0]
    
    q=qs[0]
    v=vs[0]
    dri = g.periodicRelativePositionTensor(q, boxsize)
    disti = g.distanceTensor(dri)
    
    Fin = np.zeros_like(q)
    """
    for index in indicesBonded:
        fb = g.bondForce(dri, disti, index[0], index[1], constantsBonded[index[2]])
        Fin[:,(index[0],index[1]),:] += fb
    
    for index in indicesAngle:
        fa=g.angleForce(dri,disti,index[0],index[1],index[2],[math.radians(constantsAngle[index[3]][0]),constantsAngle[index[3]][1]])
        Fin[:,(index[0],index[1],index[2]),:] += fa
    for index in indicesDihedral:
       fdih=g.dihedralForce(dri,index[0],index[1],index[2],index[3],constantsDihedral[index[4]])# g.
        Fin[:,(index[0],index[1],index[2],index[3]),:]+= fdih
    """
    #index=indicesAngle[-2]
    #fa=g.angleForce(dri,disti,index[0],index[1],index[2],[math.radians(constantsAngle[index[3]][0]),constantsAngle[index[3]][1]])
    #Fin[:,(index[0],index[1],index[2]),:] += fa
    fLJ=monoLjForce2(q,sigmaLJ,epsilonLJ,boxsize) #g.
    A = (Fin + fLJ)/m[:,np.newaxis]
    #A=Fin/m[:,np.newaxis] # conversion factor for length in nm, time in ps, and energy in kJ/mol
    return [A]


def indexMonoLj(epsilon,i,j,n_atoms):
    """
    a function used in monoLjForce2. 
    input:
        epsilon = list of minimum energies of each atom in molecule (same input is of monoLjForce2)
        i = index of atom i (w.r.t. to all atoms)
        j = index of atom j
        n_atoms = number of atoms in a molecule
    return:
        scaling of epsilon's (if atom i and j are from a different molecule), or
        0 (if atom i and j are from same molecule)
        
    """
    if np.abs(i-j)>=n_atoms:
        return np.sqrt(epsilon[np.mod(i,n_atoms)]*epsilon[np.mod(j,n_atoms)])
    else: 
        return 0
   

def monoLjForce2(q,sigma,epsilon,boxsize):
    """
    LJ force
    input:
        q = np.array([#molecule, #atom, (x,y,z)])
        sigma = list of equilibrium distances of each atom in molecule, e.g. sigma=[LjsigmaO,LjsigmaH,LjsigmaH]
        epsilon= list of minimum energies of each atom in molecule, e.g. epsilon=[LjepsilonO,LjepsilonH,LjepsilonH]
    return:
        np.array([#molecule,#atom,(Fx,Fy,Fz)])
    """
    Q=np.reshape(q,[len(q)*len(q[0]),3])
    dr=g.relativePositionTensor(Q)#,boxsize)
    R=g.distanceTensor(dr)+np.diag(np.ones(len(Q))) # this addition is done to prevent dividing by zero. No effect on end result.
    n_atoms=len(q[0])
    
    S=np.array([[(sigma[np.mod(i,n_atoms)]+sigma[np.mod(j,n_atoms)])/2 for j in range(len(Q))] for i in range(len(Q))])
    E=np.array([[indexMonoLj(epsilon,i,j,n_atoms) for j in range(len(Q))] for i in range(len(Q))])
    f= 24*E[...,np.newaxis]*(S[...,np.newaxis]**6/R[...,np.newaxis]**(8) - 2*S[...,np.newaxis]**12/R[...,np.newaxis]**14)*dr
    return  np.reshape(np.sum(f,axis=1),[len(q),len(q[0]),3]) # returns LJ forces of each atom in each molecule. 
