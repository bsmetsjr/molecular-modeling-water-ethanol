# -*- coding: utf-8 -*-
"""

Physics for water-ethanol mix.
"""
import numpy as np
import math
from math import pi
import geometry as g
import water
import ethanol
# masses of water and ethanol
m_water=np.array([15.9994,1.0080,1.0080])
m_ethanol=np.array([12.011,1.008,1.008,1.008,12.011,1.008,1.008,16,1.008])

# water specific LJ parameters
waterSigmaLJ=[0.315061,0,0]
waterEpsilonLJ=[0.66386,0,0]

# ethanol specific Lj parameters
ethanolSigmaLJ=[0.35,0.25,0.25,0.25,0.35,0.25,0.25,0.312,0]
ethanolEpsilonLJ=[0.276144,0.12552,0.12552,0.12552,0.276144,0.12552,0.12552,0.71128,0]


# Boltzmann constant for kJ/(mol K)
kB = 0.0083144621

def createInitialQV(boxsize, nMoleculesWater, nMoleculesEthanol, T):
    q_water, v_water=water.createInitialQV(boxsize*2/3, nMoleculesWater, T)
    q_ethanol, v_ethanol= ethanol.createInitialQV(boxsize/3,nMoleculesEthanol,T)
    return [q_water,q_ethanol+np.array([boxsize*2/3,0,0])], [v_water,v_ethanol]



def createInitialQV2(boxsize, nWater, nEthanol, T):
    """
Initial qs,vs structure in a cube at the given temperature.
    boxsize: size of the cube (nm)
    nWater: number of molecules of water
    nEthanol: number of molecules of ethanol
    T: initial temperature (K)
    
    returns:
        qs: [np.array[nWater, 3, (x,y,z)], np.array[nEthanol, 3, (x,y,z)]]
        vs: [np.array[nWater, 3, (vx,vy,vz)], np.array[nEthanol, 3, (vx,vy,vz)]]
    """
    # volume ratio
    vv = (nEthanol/10.0)/(nWater/33.0+nEthanol/10.0)
    
    wgridsize = np.ceil((nWater/(1-vv))**(1/3))
    egridsize = np.ceil((nEthanol/vv)**(1/3))
    wgap = boxsize/wgridsize
    egap = boxsize/egridsize
    
    swl = np.arange(wgap/2, boxsize, wgap)
    swz = np.arange(wgap/2, boxsize*(1-vv), 0.90*wgap)
    sel = np.arange(boxsize-egap/2, 0, -egap)
    sez = np.arange(boxsize-egap/3, boxsize*(1-vv), -egap)
    
    xw,yw,zw = [c.flatten()[0:nWater] for c in np.meshgrid(swl, swl, swz)]
    xe,ze,ye = [c.flatten()[0:nEthanol] for c in np.meshgrid(sel, sel, sez)]
    
    # fix oxygen at the grid-points and offset the hydrogens
    qO = np.array([xw,yw,zw]).transpose()
    qH1 = qO - np.array([math.cos(pi/2-water.Ta/2)*water.Rb, math.sin(pi/2-water.Ta/2)*water.Rb, 0])
    qH2 = qO - np.array([-math.cos(pi/2-water.Ta/2)*water.Rb, math.sin(pi/2-water.Ta/2)*water.Rb, 0])
    qw = np.stack((qO, qH1, qH2), axis=1)
    
    
    ex = np.tile(np.array([1,0,0]), nEthanol).reshape(nEthanol,3)
    ey = np.tile(np.array([0,1,0]), nEthanol).reshape(nEthanol,3)
    ez = np.tile(np.array([0,0,1]), nEthanol).reshape(nEthanol,3)
    sq = np.sqrt(2)/2
    q1 = np.array([xe,ye,ze]).transpose()
    q2 = q1 - ethanol.Rb21*sq*ey - ethanol.Rb21*sq*ez
    q3 = q1 - ethanol.Rb31*sq*ex + ethanol.Rb31*sq*ez
    q4 = q1 + ethanol.Rb41*sq*ey - ethanol.Rb41*sq*ez
    q5 = q1 + ethanol.Rb51*ex
    q6 = q5 + ethanol.Rb65*sq*ey + ethanol.Rb65*sq*ez 
    q7 = q5 - ethanol.Rb75*sq*ey + ethanol.Rb75*sq*ez
    q8 = q5 + ethanol.Rb85*sq*ex - ethanol.Rb85*sq*ez
    q9 = q8 + ethanol.Rb98*sq*ex + ethanol.Rb98*sq*ey
    
    qe = np.stack((q1, q2, q3, q4, q5, q6, q7, q8, q9), axis=1)
    
    
    
    # generate random velocity vectors and scale to temperature
    vw_base = np.random.normal(loc=0.0, scale=1.0, size=(qw.shape[0],3))
    vw = np.random.normal(loc=0.0, scale=0.5, size=qw.shape)
    vw = (vw_base[:,None,:]+vw)*np.sqrt(T/water.temperature(vw))
    
    ve_base = np.random.normal(loc=0.0, scale=1.0, size=(qe.shape[0],3))
    ve = np.random.normal(loc=0.0, scale=0.5, size=qe.shape)
    ve = (ve_base[:,None,:]+ve)*np.sqrt(T/ethanol.temperature(ve))
    
    return [qw, qe], [vw, ve]


def avgK(vs):
    """
Calculate average kinetic energy
    """
    nw = 3*vs[0].shape[0]
    ne = 9*vs[1].shape[0]
    return (nw*water.avgK(vs[0])+ne*ethanol.avgK(vs[1]))/(nw+ne)


def temperature(vs):
    """
Calculate average temperature
    """
    return 2/3 * avgK(vs) /kB


def isokineticThermostat(qs, vs, T):
    """
Isokinetic thermostat, scale the velocities to obtain temperature T.
    """
    T0 = temperature(vs)
    return qs, [vs[0]*np.sqrt(T/T0), vs[1]*np.sqrt(T/T0)]


def accelerationFunction(qs,vs,boxsize):
    """
    Computes acceleration vector for each atoms
    input:
        qs= 2-list of ni,ki,3-arrays of the positions of the atoms, where
            ni is the number of molecules of type i,
            ki is number of atoms per molecules of type i,
            3 stands for the xyz-coordinates of the position.
        vs = 2 list of ni,ki,3-arrays of velocities of atoms.
        boxsize= size of the simulation box in nm
    return:
        [a_water, a_ethanol], where a_* is the ni,ki,3-array of accelerations of molecules of type i.
    """
    q_water=qs[0]
    v_water=vs[0]
    q_ethanol=qs[1]
    v_ethanol=vs[1]
    # first compute accelerations of water and ethanol, driven by only water-, resp. ethanol-forces
    
    awi = water.accelerationFunction([q_water],[v_water],boxsize)[0]
    aei = ethanol.accelerationFunction2([q_ethanol],[v_ethanol],boxsize)[0]
    
    
    # LJ forces water <-> ethanol
    qw = q_water.reshape(q_water.shape[0]*q_water.shape[1],q_water.shape[2])
    qe = q_ethanol.reshape(q_ethanol.shape[0]*q_ethanol.shape[1],q_ethanol.shape[2])
    drwe = g.periodicMixedRelativePositionTensor(qw, qe, boxsize)
    distwe = g.distanceTensor(drwe)
    
    
    sigmaMix = 1/2*(water.LjSigmas[:,None]+ethanol.LjSigmas[None,:])
    epsilonMix = np.outer(water.LjEpsilons, ethanol.LjEpsilons)
    A = 4*epsilonMix*sigmaMix**12
    B = 4*epsilonMix*sigmaMix**6    
    A = np.tile(A, (q_water.shape[0], q_ethanol.shape[0]))
    B = np.tile(B, (q_water.shape[0], q_ethanol.shape[0]))
    
    lj = g.ljForce(drwe, distwe, A, B)
    ljWater = np.sum(lj, axis=1).reshape(q_water.shape)
    ljEthanol = np.sum(lj, axis=0).reshape(q_ethanol.shape)
    ljWater = (1e-6)*ljWater/water.m[:,None]
    ljEthanol = -(1e-6)*ljEthanol/ethanol.m[:,None]
    
    
    #fLJ=dualLjForce(qs, waterSigmaLJ, waterEpsilonLJ, ethanolSigmaLJ, ethanolEpsilonLJ,boxsize)
    #a[0]+= fLJ[0]/m_water[:,np.newaxis]
    #a[1]+= fLJ[1]/m_ethanol[:,np.newaxis]
    return [awi+ljWater, aei+ljEthanol]



def potentialEnergy(qs,vs,boxsize):
    """
    Computes potential energy of the mixture
    Returns two lists, one for each species containing potential energy per molecule.
    """
    q_water=qs[0]
    v_water=vs[0]
    q_ethanol=qs[1]
    v_ethanol=vs[1]    
   
    
    # LJ forces water <-> ethanol
    qw = q_water.reshape(q_water.shape[0]*q_water.shape[1],q_water.shape[2])
    qe = q_ethanol.reshape(q_ethanol.shape[0]*q_ethanol.shape[1],q_ethanol.shape[2])
    drwe = g.periodicMixedRelativePositionTensor(qw, qe, boxsize)
    distwe = g.distanceTensor(drwe)
    
    
    sigmaMix = 1/2*(water.LjSigmas[:,None]+ethanol.LjSigmas[None,:])
    epsilonMix = np.outer(water.LjEpsilons, ethanol.LjEpsilons)
    A = 4*epsilonMix*sigmaMix**12
    B = 4*epsilonMix*sigmaMix**6    
    A = np.tile(A, (q_water.shape[0], q_ethanol.shape[0]))
    B = np.tile(B, (q_water.shape[0], q_ethanol.shape[0]))
    
    Vlj = g.ljPotential(drwe, distwe, A, B)
    Vmix1 = np.sum(Vlj, axis=1).reshape(q_water.shape[:2])
    Vmix1 = np.sum(Vmix1, axis=-1)
    Vmix2 = np.sum(Vlj, axis=0).reshape(q_ethanol.shape[:2])
    Vmix2 = np.sum(Vmix2, axis=-1)
    
    Vwater = water.potentialEnergy([q_water], [v_water], boxsize)
    Vethanol = ethanol.potentialEnergy([q_ethanol], [v_ethanol], boxsize)
    
    
    return [Vwater+Vmix1, Vethanol+Vmix2]

"""
    LJ force between molecules of two different species
input:
    qs= 2-list of ni,ki,3-arrays of the positions of the atoms, where
            ni is the number of molecules of type i,
            ki is number of atoms per molecules of type i,
            3 stands for the xyz-coordinates of the position.
    *LJ= list of LJ constants
return:
    [F_LJ_water, F_LJ_ethanol], where F_LJ_*=np.array([#molecules of species, #atoms, (Fx,Fy,Fz)]) such that
            F_LJ_* is LJ-force induced by the molecules of the other species.
"""
def dualLjForce(qs, wSigmaLJ, wEpsilonLJ, eSigmaLJ, eEpsilonLJ,boxsize):
    mergeLocation=len(qs[0])*len(qs[0][0])
    nAtoms=[len(qs[0][0]),len(qs[1][0])]
    Q_water=np.reshape(qs[0],[len(qs[0])*len(qs[0][0]),3])
    Q_ethanol=np.reshape(qs[1],[len(qs[1])*len(qs[1][0]),3])
    Q=np.concatenate((Q_water,Q_ethanol))
    
    dr=g.periodicRelativePositionTensor(Q,boxsize)#,boxsize)
    R=g.distanceTensor(dr)+np.diag(np.ones(len(Q))) # this addition is done to prevent dividing by zero. No effect on end result.
    
    S=np.array([[sigmaIndex(wSigmaLJ,eSigmaLJ,mergeLocation,nAtoms,i,j) for j in range(len(Q))] for i in range(len(Q))])
    E= np.array([[epsilonIndex(wEpsilonLJ,eEpsilonLJ,mergeLocation,nAtoms,i,j) for j in range(len(Q))] for i in range(len(Q))])
    
    f= 24*E[...,np.newaxis]*(S[...,np.newaxis]**6/R[...,np.newaxis]**(8) - 2*S[...,np.newaxis]**12/R[...,np.newaxis]**14)*dr
    f=np.sum(f,axis=1)
    
    f_water=f[:mergeLocation]
    f_ethanol=f[mergeLocation:]
    
    return [np.reshape(f_water,[len(qs[0]),len(qs[0][0]),3]),np.reshape(f_ethanol,[len(qs[1]),len(qs[1][0]),3])]

def sigmaIndex(wSigma, eSigma, merged_loc,nAtoms,i,j):
    """
    used in dualLjForce, returns zero if atoms i and j are from same molecule-species.
    input:
        wSigma = list of LJ-equilibrium distances for water
        eSigma = list of LJ-equilibrium distances for ethanol
        merged_loc =  index in list of all atoms Q, where ethanol molecules start
        nAtoms= list containing the number of atoms in a molecule per species
        i, j = given index.
    returns:
        0, if atoms i and j are from same molecule species, or
        (wSigma[np.mod(i,nAtoms[0])]+eSigma[np.mod(j,nAtoms[1])])/2, if atom i is water, or
        (wSigma[np.mod(j,nAtoms[0])]+eSigma[np.mod(i,nAtoms[1])])/2, if atom i is ethanol.
        
    """
    if (i<merged_loc and j>=merged_loc) or (i>=merged_loc and j<merged_loc):
        if i<merged_loc:
            return (wSigma[np.mod(i,nAtoms[0])]+eSigma[np.mod(j,nAtoms[1])])/2
        else:
            return (wSigma[np.mod(j,nAtoms[0])]+eSigma[np.mod(i,nAtoms[1])])/2
    else:
        return 0

def epsilonIndex(wEpsilon, eEpsilon, merged_loc,nAtoms,i,j):
    """
    used in dualLjForce, returns zero if atoms i and j are from same molecule-species.
    input: 
        *same as of sigmaIndex()*
    return:
        0, if atoms i and j are from same molecule species, or
        np.sqrt(wEpsilon[np.mod(i,nAtoms[0])]*eEpsilon[np.mod(j,nAtoms[1])]), if atom i is water, or
        np.sqrt(wEpsilon[np.mod(j,nAtoms[0])]*eEpsilon[np.mod(i,nAtoms[1])]), if atom i is ethanol.
        
    """
    if (i<merged_loc and j>=merged_loc) or (i>=merged_loc and j<merged_loc):
        if i<merged_loc:
            return np.sqrt(wEpsilon[np.mod(i,nAtoms[0])]*eEpsilon[np.mod(j,nAtoms[1])])
        else:
            return np.sqrt(wEpsilon[np.mod(j,nAtoms[0])]*eEpsilon[np.mod(i,nAtoms[1])])
    else:
        return 0