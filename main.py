# -*- coding: utf-8 -*-
"""

This is the main file of the project. It runs the simulation of all systems under consideration, 
outputs the resulting trajectory as a .xyz file, and performs analysis of a number of physical 
quantities throughout the time steps.

For a list of the specific packages used and other information about e.g. the file structure,
data structures and copyrights, see the README.md file.

We make use of the following units:
    length in nanometer (nm)
    time in femtosecond (fs)
    mass in atomic mass unit (amu)
    Temperature in Kelvin (K)
"""
import numpy as np

import integrators
import utils
import ethanol
import water
import waterethanolmix as mix
from tqdm import tqdm


dt = 1.0 # in fs
t_end=1.0e4 # in fs
T=300 # in K
boxsize = 3.0 # in nm



"""
First, running a system of water molecules.
"""
nWaterMolecules = int(33*boxsize**3)

q_water,v_water = water.createInitialQV(boxsize, nWaterMolecules, T) # (nm, K)
qs_water=[q_water]
vs_water=[v_water]

qst_water = [qs_water]
vst_water = [vs_water]

KinEnergyWater=[] # list of average kinetic energies.

for i in tqdm(range(int(t_end/dt)), desc='Simulating water', ncols=80):
    qs_water, vs_water = integrators.velocityVerlet(qs_water, vs_water, 
        water.accelerationFunction, dt, boxsize)
    qs_water, vs_water = water.isokineticThermostat(qs_water[0], vs_water[0], T)
    qs_water, vs_water = [qs_water], [vs_water]
    qst_water.append(qs_water)
    vst_water.append(vs_water)
    KinEnergyWater.append(water.avgK(vs_water[0]))
    # perhaps some statistics as well.
    
    
#write results
utils.Output_xyz(qst_water, 'main_water_q.xyz', water.atomNames)
utils.Output_xyz(vst_water, 'main_water_v.xyz', water.atomNames)

"""
Next, a system of ethanol molecules
"""
nEthanolMolecules = int(10*boxsize**3)

q_ethanol,v_ethanol = ethanol.createInitialQV(boxsize, nEthanolMolecules, T) # (nm, K)
qs_ethanol=[q_ethanol]
vs_ethanol=[v_ethanol]

qst_ethanol = [qs_ethanol]
vst_ethanol = [vs_ethanol]

for i in tqdm(range(int(t_end/dt)), desc='Simulating ethanol', ncols=80):
    qs_ethanol, vs_ethanol = integrators.velocityVerlet(qs_ethanol, vs_ethanol, 
        ethanol.accelerationFunction2, dt, boxsize)
    qs_ethanol, vs_ethanol = ethanol.isokineticThermostat(qs_ethanol[0], vs_ethanol[0], T)
    qs_ethanol, vs_ethanol = [qs_ethanol], [vs_ethanol]
    qst_ethanol.append(qs_ethanol)
    vst_ethanol.append(vs_ethanol)
    
# write results
utils.Output_xyz(qst_ethanol, 'main_ethanol_q.xyz', ethanol.atomNames)
utils.Output_xyz(vst_ethanol, 'main_ethanol_v.xyz', ethanol.atomNames)



"""
The ethanol-water mixture.
"""
vvPercentageEthanol = 0.135
nWaterMolecules = int((1-vvPercentageEthanol)*33*boxsize**3)
nEthanolMolecules = int(vvPercentageEthanol*10*boxsize**3)

qs_mix,vs_mix= mix.createInitialQV2(boxsize,nWaterMolecules,nEthanolMolecules,T) # (nm, K)

qst_mix = [qs_mix]
vst_mix = [vs_mix]

for i in tqdm(range(int(t_end/dt)), desc='Simulating water/ethanol', ncols=80):
    qs_mix, vs_mix = integrators.velocityVerlet(qs_mix, vs_mix, 
        mix.accelerationFunction, dt, boxsize)
    qs_mix, vs_mix = mix.isokineticThermostat(qs_mix, vs_mix, T)
    qst_mix.append(qs_mix)
    vst_mix.append(vs_mix)



# write results
utils.Output_xyz2(qst_mix,'main_mix_q.xyz', [water.atomNames,ethanol.atomNames])
utils.Output_xyz2(vst_mix,'main_mix_v.xyz', [water.atomNames,ethanol.atomNames])

print('Done, results can be found in the working directory as main_*.xyz files')

"""
Some statistics
"""




